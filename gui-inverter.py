import sys, builtins
from classes.inv_control import InvControl
from classes.inv_control_canopen import InvControlCanopen
from classes.inv_control_acs import InvControlAcs
from classes.inv_control_BnR import InvControlBnR
from classes.inv_frontend import InvFrontend
from classes.macro_map import MacroMapDialog
from classes.automation_tests.inv_torqueAngleConfig import TorqueAngleConfig
from classes.automation_tests.inv_mechanicalChartConfig import MechChartTestConfig
from classes.inv_frontend_about import AboutDialog
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QHBoxLayout, QMessageBox, qApp
from classes.analysis.inv_analysis_main import InvAnalysis
builtins.APP_NAME = "GUI Inverter"
APP_NAME = "GUI Inverter"

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, \
        QComboBox, QLabel
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QAction
from PyQt5.QtCore import Qt

invChooseList = {
    0: {
        "title":    "(brak)",
        "obj":      None
    },
    #1: {
    #    "title":    "Symulator",
    #    "obj":      InvControl
    #},
    1: {
        "title":    "CANopen",
        "obj":      InvControlCanopen
    },
    2: {
        "title":    "Modbus RTU",
        "obj":      InvControl
    },
    3: {
        "title":    "ACS 800 Modbus TCP",
        "obj":      InvControlAcs
    },
    4: {
        "title":    "BnR DLC Modbus TCP",
        "obj":      InvControlBnR
    }
}

class InvChooser(QDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.setWindowTitle(APP_NAME)
        layout = QVBoxLayout()
        
        cbList  = dict()

        pixmap = QPixmap("photo/logo-removebg-preview.png")

        labelImage = QLabel()
        labelImage.setPixmap(pixmap)
        layout.addWidget(labelImage)

        for i in range(2):
            cbInv   = QComboBox()
            cbPort  = QComboBox()
            cbPort.setEnabled(False)
            
            for n in range(len(invChooseList)):
                cbInv.addItem(invChooseList[n].get("title"))

            cbInv.currentIndexChanged.connect(self.__cbChanged)
            cbInv.cbPort  = cbPort
            cbList[i] = cbInv

            lbl = QLabel("Wybierz połączenie z falownikiem {}:".format(i + 1))
            #QLabel("Falownik {}:".format(i + 1))
            hl = QHBoxLayout()
            hl.setAlignment(Qt.AlignCenter | Qt.AlignCenter)

            hl.addWidget(lbl, 1)
            hl.addWidget(cbInv,  1)
            hl.addWidget(cbPort, 1)
            layout.addLayout(hl)
            
        buttonBox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        layout.addWidget(buttonBox)
        self.setLayout(layout)

        if self.exec_() == QDialog.Accepted:
            self.invList = dict()
            
            i = 0
            for cbInv in cbList.values():
                item  = invChooseList[cbInv.currentIndex()]
                
                if item.get("obj") is None:
                    continue
                portList = cbInv.cbPort.portList
                try:
                    if portList is not None:
                        port = portList[cbInv.cbPort.currentIndex()].get("port")
                    else:
                        port = None
                except:
                    QMessageBox.warning(self, APP_NAME,
                                        "Incorrect port selection.")
                    return

                self.invList[i] = dict(item, port=port)
                i += 1
            return
        else:
            sys.exit()

    def __cbChanged(self, val):
        cb  = self.sender().cbPort
        obj = invChooseList[val].get("obj")
        
        cb.clear()
        
        if obj is not None:
            portList  = obj.getPortList()
            
            if portList is not None:
                for p in portList.values():
                    cb.addItem(p.get("title"))
                
                cb.portList = portList
                cb.setEnabled(True)
                return
        
        cb.portList = None
        cb.setEnabled(False)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.layout = QHBoxLayout()

        invList = InvChooser().invList
        self.invFrontendList  = dict()
        
        for item in invList.values():
            invCtrl = item["obj"](port=item["port"])
            invFrontend = InvFrontend(invCtrl, title=item["title"])
            self.layout.addWidget(invFrontend)

            self.invFrontendList[len(self.invFrontendList)] = invFrontend

        self.widget = QWidget()
        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)

        menuBar = self.menuBar()
        self.statusbar = self.statusBar()
        self.statusbar.showMessage('Ready')

        app = menuBar.addMenu("&File")
        #newConfig = QAction("New configuration", self)
        #newConfig.triggered.connect(self.__newConfiguration)
        #app.addAction(newConfig)

        #newConfig = QAction("Save", self)
        #app.addAction(newConfig)

        newConfig = QAction("Exit", self)
        newConfig.triggered.connect(self.__closeApp)
        app.addAction(newConfig)

        #macroMenu = menuBar.addMenu("&Macros")
        testsMenu = menuBar.addMenu("&Macros")

        voltageControlMode = QAction("Voltage control mode", self)
        voltageControlMode.triggered.connect(self.__runVolgateControlMode)
        voltageControlMode.setChecked(False)
        testsMenu.addAction(voltageControlMode)

        torqeAngleTest = QAction("Current control mode (Theta set)", self)
        torqeAngleTest.triggered.connect(self.__runTorqueAngleTest)
        torqeAngleTest.setChecked(False)
        testsMenu.addAction(torqeAngleTest)

        currentControlMode = QAction("Current control mode (Ramp gen)", self)
        currentControlMode.triggered.connect(self.__runCurrentControlModeRampGen)
        currentControlMode.setChecked(False)
        testsMenu.addAction(currentControlMode)

        currentControlModeFoc = QAction("Current control mode (FOC)", self)
        currentControlModeFoc.triggered.connect(self.__runCurrentControlModeFOC)
        currentControlModeFoc.setChecked(False)
        testsMenu.addAction(currentControlModeFoc)

        torqueControlMode = QAction("Torque control mode", self)
        torqueControlMode.triggered.connect(self.__runTorqueControlMode)


        torqueControlMode.setChecked(False)
        testsMenu.addAction(torqueControlMode)

        mechChartTest = QAction("Mechanical chart", self)
        mechChartTest.triggered.connect(self.__runMechChartMode)
        mechChartTest.setChecked(False)
        testsMenu.addAction(mechChartTest)

        analysis = menuBar.addMenu("A&nalysis")
        anMechChart = QAction("Mechanical chart analysis", self)
        anMechChart.triggered.connect(self.__analysisMechChart)
        anMechChart.setChecked(False)
        analysis.addAction(anMechChart)

        about = menuBar.addMenu("&Help")
        aboutInvCtrl = QAction("About", self)
        aboutInvCtrl.triggered.connect(self.__runAbout)
        aboutInvCtrl.setChecked(False)
        about.addAction(aboutInvCtrl)

        self.setWindowTitle("Main menu")
        self.show()


    def __newConfiguration(self):
        pass
    def __runMapMacro(self):
        MacroMapDialog(self.invFrontendList)
    def __runTorqueAngleTest(self):
        TorqueAngleConfig(self.invFrontendList)
    def __runMechChartMode(self):
        MechChartTestConfig(self.invFrontendList)
    def __runVolgateControlMode(self):
        QMessageBox.warning(self, "Error", "Option not available.")
    def __runCurrentControlModeRampGen(self):
        QMessageBox.warning(self, "Error", "Option not available.")
    def __runCurrentControlModeFOC(self):
        QMessageBox.warning(self, "Error", "Option not available.")
    def __runTorqueControlMode(self):
        QMessageBox.warning(self, "Error", "Option not available.")

    def __analysisMechChart(self):
            InvAnalysis()

    def __runAbout(self):
       AboutDialog()
    def __closeApp(self):
        sys.exit()

app = QApplication(sys.argv)
w   = MainWindow()
sys.exit(app.exec_())
