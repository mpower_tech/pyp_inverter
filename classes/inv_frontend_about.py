from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QLabel

class AboutDialog(QDialog):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("About")
        pixmap = QPixmap("photo/about.PNG")

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(p)

        labelImage = QLabel(self)
        labelImage.setPixmap(pixmap)

        labelVersion = QLabel("V07.10.2020")
        labelVersion.setAlignment(Qt.AlignRight)
        labelVersion.setStyleSheet("QLabel {color:blue}")

        layout = QVBoxLayout()
        layout.addWidget(labelImage)
        layout.addWidget(labelVersion)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        buttonBox.accepted.connect(self.accept)

        layout.addWidget(buttonBox)
        self.setLayout(layout)

        self.exec_()