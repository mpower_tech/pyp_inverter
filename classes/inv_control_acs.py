import socket
from umodbus import conf
from umodbus.client import tcp

class InvControlAcs:
    def __init__(self, **kwargs):
        self.regsArray = [0] * 48
        self.goodFramesCount  = 0
        self.badFramesCount   = 0

        # Enable values to be signed (default is False).
        conf.SIGNED_VALUES = True

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connected = False
        self.controlWord = 0
        self.regsArray[45] = 1
        self.counter = 0
        self.firstConnection = True

        self.readActStates = tcp.read_holding_registers(slave_id=1, starting_address=101, quantity=7) #Vel,Freq,Curr,Tqr,Pwr,DCBus,MainVlg
        self.readWords = tcp.read_holding_registers(slave_id=1, starting_address=300, quantity=3) # CtrlWord,StatusWord,AuxStatWord
        self.readTorq = tcp.read_holding_registers(slave_id=1, starting_address=2003, quantity=1)
        #self.writeParams = tcp.write_multiple_registers(slave_id=1, starting_address=0, values=[0, 0, 0]) #self.ctnWord,self.vel,self.torque

    def set_bit(self, value, bit):
        return value | (1 << bit)

    def clear_bit(self, value, bit):
        return value & ~(1 << bit)

    def getMbusData(self):
        try:
            if self.connected:
                data = tcp.send_message(self.readActStates, self.sock)
                for i in range(0, 7):
                    if i == 0:
                        self.regsArray[i] = data[i] / 20000.0 * 3000
                    elif i == 1:
                        self.regsArray[i] = data[i] / 100
                    elif i == 2:
                        self.regsArray[i] = data[i] / 10
                    elif i == 3:
                        self.regsArray[i] = data[i] / 1000.0 * (9550.0 * 54.4 / 4000.0)
                    else:
                        self.regsArray[i] = data[i]

                data = tcp.send_message(self.readTorq, self.sock)
                self.regsArray[42] = data[0]/100

                data = tcp.send_message(self.readWords, self.sock)
                self.regsArray[7] = data[1]

                self.counter += 1
                if self.counter > 3:
                    self.counter = 0
                    self.regsArray[45] = 0

                if self.firstConnection:
                    self.firstConnection = False
                    self.readSetValue = tcp.read_holding_registers(slave_id=1, starting_address=0, quantity=3)
                    data = tcp.send_message(self.readSetValue, self.sock)

                    self.regsArray[37] = data[1]/10
                    self.regsArray[35] = data[2]/10

                self.goodFramesCount += 1
            else:
                try:
                    if not self.connected: # Inny blad wysylania
                        self.sock.settimeout(0.1)
                        self.sock.connect('192.168.0.3', 502)
                        self.connected = True
                        self.firstConnection = True
                        self.regsArray[45] = 1
                except:
                    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.sock.settimeout(0.1)

        except:
            self.connected = False
            self.regsArray[7] = 0
            self.badFramesCount += 1

    def getReg(self, name):
        if not name in self.registerDefs:
            print("get nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        val   = self.regsArray[addr - 4001]
        # print("get reg \"{}\" = {}".format(name, val))

        return val

    def setReg(self, name, val):
        if not name in self.registerDefs:
            print("set nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]

        self.regsArray[addr - 4001]  = int(val)
        #print("set reg \"{}\" = {}".format(name, int(val)))

        try:
            if self.connected:
                if name == "MB_SP_TqrLim":
                    self.write = tcp.write_single_register(slave_id=1, address=2003, value=int(val)*100)
                    tcp.send_message(self.write, self.sock)

                if name == "MB_SP_Iq":
                    self.write= tcp.write_single_register(slave_id=1, address=2, value=int(val)*10)
                    tcp.send_message(self.write, self.sock)

                if name == "MB_SP_Speed":
                    self.write = tcp.write_single_register(slave_id=1, address=1, value=int(val)*10)
                    tcp.send_message(self.write, self.sock)

                if name == "MB_InverterMode":

                    if val == 1:
                        self.controlWord = self.set_bit(self.controlWord, 11)
                    else:
                        self.controlWord = self.clear_bit(self.controlWord, 11)

                    self.write = tcp.write_single_register(slave_id=1, address=0, value=self.controlWord)
                    tcp.send_message(self.write, self.sock)

                if name == "MB_Command":
                    if int(val) == 1:
                        tabRes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        for numOfBits in tabRes:
                            self.controlWord = self.clear_bit(self.controlWord, numOfBits)

                    elif int(val) == 2:
                        tabSet = [0, 1, 2, 10]
                        tabRes = [3, 4, 5, 6, 7, 8, 9]

                        for numOfBits in tabSet:
                            self.controlWord = self.set_bit(self.controlWord, numOfBits)
                        for numOfBits in tabRes:
                            self.controlWord = self.clear_bit(self.controlWord, numOfBits)

                    elif int(val) == 4:
                        # wyslac po prostu tylko reset. Ale pierw sprawdzic czy jest blad.
                        if bool(self.regsArray[7] & 0x08):
                            self.controlWord = 128

                    elif int(val) == 8:  # start
                        tabSet = [3, 4, 5, 6]

                        for numOfBits in tabSet:
                            self.controlWord = self.set_bit(self.controlWord, numOfBits)

                    elif int(val) == 16:  # stop
                        if self.controlWord & 0b11000:
                            tabRes = [3, 4, 5, 6]
                            for numOfBits in tabRes:
                                self.controlWord = self.clear_bit(self.controlWord, numOfBits)

                    self.write = tcp.write_single_register(slave_id=1, address=0, value=self.controlWord)
                    tcp.send_message(self.write, self.sock)
                    self.controlWord = self.clear_bit(self.controlWord, 7)

                self.goodFramesCount += 1
                self.connected = True

        except:
            self.badFramesCount += 1
            self.connected = False

        return True

    def readStatusRegs(self):
        self.goodFramesCount = self.goodFramesCount # += 1

    def getConnType(self):
        if self.connected:
            return "Modbus TCP | CONNECTED"
        else:
            return "Modbus TCP | NOT CONNECTED"
    
    def getGoodFramesCount(self):
        return self.goodFramesCount

    def getBadFramesCount(self):
        return self.badFramesCount

    @staticmethod
    def getPortList():
        return None

    registerDefs = {
        "MB_Inverter_State":    4015,
        "MB_Inverter_Power":    4016,
        "MB_Act_Speed":         4001,
        "MB_Act_DcBus_U":       4002,
        "MB_Act_DcBus_I":       4003,
        "MB_Act_Iq":            4004,
        "MB_Act_Power":         4005,
        "MB_Poles":             4007,
        "MB_Global_Status":     4008,
        "MB_Control_Word":      4045,
        "MB_FirstCnt":          4046,
        "MB_Act_PosMech":       4012,
        "MB_Command":           4030,
        "MB_InverterMode":      4031,
        "MB_Dac1Cfg":           4032,
        "MB_Dac2Cfg":           4033,
        "MB_Dac3Cfg":           4034,
        "MB_Dac4Cfg":           4035,
        "MB_SP_Iq":             4036,
        "MB_SP_Id":             4037,
        "MB_SP_Speed":          4038,
        "MB_SP_PosMech":        4039,
        "MB_SP_TqrLim":         4043
    }
    
    frontendCaps = {
        "command": {
            "title":    "Command",
            "type":     "barCommand",
            "reg":      "MB_Command",
            "btn_grp": {
                "Power": {
                    "Enable":   0x02,
                    "Disable":  0x01
                },
                "Run": {
                    "Start":    0x08,
                    "Stop":     0x10
                },
                "Reset": {
                    "Reset Errors": 0x04
                }
            }
        },
        "config": {
            "title":    "Config",
            "type":     "barConfig",
            "configs": {
                "Mode": {
                    "reg":      "MB_InverterMode",
                    "choose": {
                        0: "Speed",
                        1: "Torque"

                    }
                },
                "Torque limit ( 0 - 100% )": {
                    "reg":      "MB_SP_TqrLim",
                    "min":      0,
                    "max":      100
                }
            }
        },
        "status": {
            "title":    "Status",
            "type":     "barStatus",
            "reg":      "MB_Global_Status",
            "flags": {
                "Power On":    0x01,
                "Is Run":      0x100,
                "Error":       0x08,
            }
        },
        "iq": {
            "title":    "Torque",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Iq",
            "act_reg":  "MB_Act_Iq",
            "unit":     "Nm",
            "min":      -10,
            "max":       10,
            "decs":       1,
            "firstCnt": "MB_FirstCnt"
        },
        "vel": {
            "title": "Velocity",
            "type": "barSetpoint",
            "reg": "MB_SP_Speed",
            "act_reg": "MB_Act_Speed",
            "unit": "RPM",
            "min": -1800,
            "max": 1800,
            "decs": 0,
            "firstCnt": "MB_FirstCnt"
        },
        "acs_params_bus": {
            "title": "Other",
            "type": "barOtherAcs",
            "pwr_regs": "MB_Act_Power",
            "cur_regs": "MB_Act_DcBus_I",
            "curr_decs": 1,
            "freq_regs": "MB_Act_DcBus_U",
            "unit": ["A", "Hz", "W"]
        },
    }
