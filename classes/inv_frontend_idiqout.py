from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel

#
# InvIqIdOut -- komponent wskaźnik wyjsciowego Iq i Id
#

class InvIqIdOut(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()

        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        
        self.regIqOut  = kwargs.get("iq_reg")
        self.regIdOut  = kwargs.get("id_reg")
        
        self.descIq = kwargs.get("iq_decs")
        self.descId = kwargs.get("id_decs")

        self.unit = kwargs.get("unit")

        self.valIq  = None
        self.valId  = None

        f = QFont("Helvetica")
        f.setPointSize(f.pointSize() + 1)
        f.setBold(True)

        self.lbIqOut  = QLabel()
        self.lbIqOut.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
        self.lbIqOut.setFont(f)

        self.lbIdOut  = QLabel()
        self.lbIdOut.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
        self.lbIdOut.setFont(f)


        self.__lbUpdate()

        layout = QHBoxLayout()
        layout.addWidget(self.lbIqOut)
        layout.addWidget(self.lbIdOut)


        self.setLayout(layout)

    def __lbUpdate(self):

        if self.valIq is not None:
            self.lbIqOut.setText("{val:.{decs}f} {unit}".format(
                val   = self.valIq,
                decs  = self.descIq,
                unit = self.unit[0]))
        else:
            self.lbIqOut.setText("-- {unit}".format(unit=self.unit[0]))

        if self.valId is not None:
            self.lbIdOut.setText("{val:.{decs}f} {unit}".format(
                val   = self.valId,
                decs  = self.descId,
                unit  = self.unit[1]))
        else:
            self.lbIdOut.setText("-- {unit}".format(unit=self.unit[1]))


    def regUpdate(self):

        self.valIq = float(self.invControl.getReg(self.regIqOut))
                        #* pow(10, -self.descIq)

        self.valId = float(self.invControl.getReg(self.regIdOut))
                        #* pow(10, -self.descId)

        self.__lbUpdate()

