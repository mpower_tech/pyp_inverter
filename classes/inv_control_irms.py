from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel

# Author: Bartlomiej Kucharczyk
# Date: 17.08.2020
# Class name: InvIrms
# Desc: This class create layout for 3 irms values

class InvIrms(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()
        self.invControl = invControl
        self.setTitle(kwargs.get("title"))

        self.regPhU = kwargs.get("phU_Irms")
        self.regPhV = kwargs.get("phV_Irms")
        self.regPhW = kwargs.get("phW_Irms")

        self.descPhU = kwargs.get("phU_Irms_decs")
        self.descPhV = kwargs.get("phV_Irms_decs")
        self.descPhW = kwargs.get("phW_Irms_decs")

        f = QFont("Helvetica")
        f.setPointSize(f.pointSize() + 1)
        f.setBold(True)

        self.valPhU = None
        self.valPhV = None
        self.valPhW = None

        self.lbPhU = QLabel()
        self.lbPhU.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbPhU.setFont(f)

        self.lbPhV = QLabel()
        self.lbPhV.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbPhV.setFont(f)

        self.lbPhW = QLabel()
        self.lbPhW.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbPhW.setFont(f)

        self.__lbUpdate()

        layout = QHBoxLayout()
        layout.addWidget(self.lbPhU)
        layout.addWidget(self.lbPhV)
        layout.addWidget(self.lbPhW)
        self.setLayout(layout)


    def __lbUpdate(self):
        if self.valPhU is not None:
            self.lbPhU.setText("{val:.{decs}f} A".format(
                val=self.valPhU,
                decs=self.descPhU))
        else:
            self.lbPhU.setText("-- A")

        if self.valPhV is not None:
            self.lbPhV.setText("{val:.{decs}f} A".format(
                val=self.valPhV,
                decs=self.descPhV))
        else:
            self.lbPhV.setText("-- A")

        if self.valPhW is not None:
            self.lbPhW.setText("{val:.{decs}f} A".format(
                val=self.valPhW,
                decs=self.descPhW))
        else:
            self.lbPhW.setText("-- A")



    def regUpdate(self):
        self.valPhU = float(self.invControl.getReg(self.regPhU)) \
                          * pow(10, -self.descPhU)

        self.valPhV = float(self.invControl.getReg(self.regPhV)) \
                        * pow(10, -self.descPhV)

        self.valPhW = float(self.invControl.getReg(self.regPhW)) \
                        * pow(10, -self.descPhW)

        self.__lbUpdate()
