import collections
from PyQt5.QtGui import QPixmap
from classes.inv_frontend_commandbar import InvCommandBar
from classes.inv_frontend_configbar import InvConfigBar
from classes.inv_frontend_statusbar import InvStatusBar
from classes.inv_frontend_setpointbar import InvSetpointBar
from classes.inv_frontend_dcbusbar import InvDcBusBar
from classes.inv_frontend_dacbar import InvDacBar
from classes.inv_frontend_connstatusbar import InvConnStatusBar
from classes.inv_control_temperature import InvTemp
from classes.inv_control_acs_otherpar import InvAcsParams
from classes.inv_frontend_errors import InvErrorBar
from classes.inv_frontend_warning import InvWarningBar
from classes.inv_control_encoder import InvEnc
from classes.inv_control_irms import InvIrms
from classes.inv_frontend_setencoder import InvEncoSet
from classes.inv_frontend_idiqout import InvIqIdOut
from classes.inv_control_otherpar import InvDF1Params
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QLabel

#
# InvFrontend - kontrolka frontend falownika
#

class InvFrontend(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()

        self.invControl   = invControl
        self.ctrlWidgets  = collections.OrderedDict()
        mainLayout = QHBoxLayout()
        mainLayout.setAlignment(Qt.AlignTop)

        layout = QVBoxLayout()
        layout.setAlignment(Qt.AlignTop)

        layout2 = QVBoxLayout()
        layout2.setAlignment(Qt.AlignTop)

        for key, caps in invControl.frontendCaps.items():
            widget = {
                "barCommand":  InvCommandBar,
                "barConfig":   InvConfigBar,
                "barEncoSet":  InvEncoSet,
                "barStatus":   InvStatusBar,
                "barErrors":   InvErrorBar,
                "barWarnings": InvWarningBar,
                "barSetpoint": InvSetpointBar,
                "barEnc":      InvEnc,
                "barIrms":     InvIrms,
                "barDcBus":    InvDcBusBar,
                "barIqIdOut":  InvIqIdOut,
                "barTemp":     InvTemp,
                "barOtherAcs": InvAcsParams,
                "barDF1":     InvDF1Params,
                "barDac":      InvDacBar,
            }.get(caps.get("type"))(invControl, **caps)

            if caps.get("type") == "barStatus" or caps.get("type") == "barErrors" or caps.get("type") == "barWarnings" or caps.get("type") == "barDcBus" \
                    or caps.get("type") == "barTemp" or caps.get("type") == "barEnc" or caps.get("type") == "barDac" or caps.get("type") == "barOtherAcs" \
                    or caps.get("type") == "barIrms" or caps.get("type") == "barIqIdOut" or caps.get("type") == "barDF1":
                layout2.addWidget(widget)
            else:
                layout.addWidget(widget)
            self.ctrlWidgets[key] = widget


        if kwargs.get("title") == 'Modbus RTU':
            labelImage = QLabel(self)
            pixmap = QPixmap("photo/mpowerSmall-removebg-preview.png")
            labelImage.setPixmap(pixmap)
            labelImage.setAlignment(Qt.AlignTop | Qt.AlignCenter)
            layout2.addWidget(labelImage)

        invStatus = InvConnStatusBar(invControl, title="Conn Status")
        layout.addWidget(invStatus)
        self.ctrlWidgets["conn_status"] = invStatus

        mainLayout.addLayout(layout)
        mainLayout.addLayout(layout2)

        self.setTitle(kwargs.get("title"))
        self.setLayout(mainLayout)

        if kwargs.get("title") == 'Modbus RTU' or kwargs.get("title") == 'ACS 800 Modbus TCP' or kwargs.get("title") == 'BnR DLC Modbus TCP':
            tmrMbus = QTimer(self)
            tmrMbus.timeout.connect(self.__getDataViaMbus)
            tmrMbus.start(500)

        tmr = QTimer(self)
        tmr.timeout.connect(self.__updateWidgets)
        tmr.start(400)
        self.__updateWidgets()

    def __updateWidgets(self):
        self.invControl.readStatusRegs()
        for key, widget in self.ctrlWidgets.items():
            widget.regUpdate()

    def __getDataViaMbus(self):
        self.invControl.getMbusData()



