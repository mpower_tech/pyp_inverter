from PyQt5.QtWidgets import QGroupBox, QRadioButton, QGridLayout, QMessageBox
import collections

#
# InvErrorBar -- komponent wskaźnik aktualnych bledow
#

class InvErrorBar(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()

        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        self.regName = kwargs.get("reg")
        self.lastAlarmList = 0

        grid_layout = QGridLayout()
        row = 0
        col = 0

        self.labelSet = collections.OrderedDict()

        for title, mask in kwargs.get("flags").items():

            qradioError = QRadioButton(title)
            qradioError.setAutoExclusive(False)
            qradioError.setStyleSheet("QRadioButton:indicator::checked"
                                      "{"
                                      "image:url(photo/Check.png);"
                                      "}"
                                      "QRadioButton:indicator::unchecked"
                                      "{"
                                      "image:url(photo/Unchecked.png);"
                                      "}")

            grid_layout.addWidget(qradioError, col, row)
            col += 1
            if col > 3:
                row += 1
                col = 0

            self.labelSet[mask] = qradioError

        self.setLayout(grid_layout)

    def regUpdate(self):
        val = self.invControl.getReg(self.regName)

        if val > self.lastAlarmList:
            QMessageBox.warning(self, "New error occur", "The error has occur. Check the errors list.")
        self.lastAlarmList = val

        for mask, label in self.labelSet.items():
           label.setChecked(bool(int(val) & mask))

