import time
import canopen

class InvControlCanopen:
    def __init__(self, **kwargs):
        self.regsArray = [0] * 48
        self.goodFramesCount  = 0
        self.badFramesCount   = 0
        self.node_id = -1
     
        self.network = canopen.Network()
        self.network.connect(channel = "vcan0", bustype = "socketcan")
        self.network.scanner.search()

        time.sleep(0.1)
        for i in self.network.scanner.nodes:
            #print("found node {}".format(i))
            self.network.add_node(i)
            self.node_id = i
        
        node = self.network[self.node_id]
        
        for name, reg in self.registerDefs.items():
            obj = canopen.objectdictionary.Variable(name, reg)
            obj.data_type = canopen.objectdictionary.INTEGER16
            node.object_dictionary.add_object(obj)
        
        node.nmt.state = 'OPERATIONAL'
        
        for name, reg in self.registerDefs.items():
            self.regsArray[reg - 0x2000] = node.sdo[reg].raw
            self.goodFramesCount += 1

    
    def getReg(self, name):
        if not name in self.registerDefs:
            print("get nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        val   = self.regsArray[addr - 0x2000]
        # print("get reg \"{}\" = {}".format(name, val))
        return val

    def setReg(self, name, val):
        if not name in self.registerDefs:
            print("set nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        self.regsArray[addr - 0x2000]  = int(val)
        # print("set reg \"{}\" = {}".format(name, int(val)))

        self.network[self.node_id].sdo[addr].raw  = int(val)
        self.goodFramesCount += 1
        return True

    def readStatusRegs(self):
        node = self.network[self.node_id]
        
        for name, reg in self.registerDefs.items():
            if reg > (0x2000 + 16):
                break;
            
            self.regsArray[reg - 0x2000] = node.sdo[reg].raw
            self.goodFramesCount += 1

    def getConnType(self):
        return "CANopen node {}".format(self.node_id)
    
    def getGoodFramesCount(self):
        return self.goodFramesCount

    def getBadFramesCount(self):
        return self.badFramesCount
    
    @staticmethod
    def getPortList():
        portList  = dict()

        for i in range(4):
            portList[i] = dict(
                port  = "port{}".format(i + 1),
                title = "node {}".format(i + 1)
            )
           # print(portList[i])
        return portList

    registerDefs = {
        "MB_Global_Status":     0x2000 +  1,
        "MB_Inverter_State":    0x2000 +  2,
        "MB_Inverter_Power":    0x2000 +  3,
        "MB_Act_Iq":            0x2000 +  7,
        "MB_Act_Id":            0x2000 +  8,
        "MB_Act_Speed":         0x2000 + 10,
        "MB_Act_PosMech":       0x2000 + 12,
        "MB_Act_DcBus_U":       0x2000 + 15,
        "MB_Act_DcBus_I":       0x2000 + 16,
        "MB_Poles":             0x2000 + 28,
        "MB_Command":           0x2000 + 30,
        "MB_InverterMode":      0x2000 + 31,
        "MB_Dac1Cfg":           0x2000 + 32,
        "MB_Dac2Cfg":           0x2000 + 33,
        "MB_Dac3Cfg":           0x2000 + 34,
        "MB_Dac4Cfg":           0x2000 + 35,
        "MB_SP_Iq":             0x2000 + 36,
        "MB_SP_Id":             0x2000 + 37,
        "MB_SP_Speed":          0x2000 + 38,
        "MB_SP_PosMech":        0x2000 + 39,
        "MB_SP_EncOffset":      0x2000 + 43
    }
    
    frontendCaps = {
        "command": {
            "title":    "Command",
            "type":     "barCommand",
            "reg":      "MB_Command",
            "btn_grp": {
                "Power": {
                    "Enable":   0x02,
                    "Disable":  0x01
                },
                "Run": {
                    "Start":    0x08,
                    "Stop":     0x10
                },
                "Reset": {
                    "Reset Errors": 0x04
                }
            }
        },
        "config": {
            "title":    "Config",
            "type":     "barConfig",
            "configs": {
                "Mode": {
                    "reg":      "MB_InverterMode",
                    "choose": {
                        0: "Torque",
                        1: "Speed",
                        2: "Position"
                    }
                },
                "Poles": {
                    "reg":      "MB_Poles",
                    "readonly": True
                },
                "Enc Offset": {
                    "reg":      "MB_SP_EncOffset",
                    "min":         0,
                    "max":      1024
                }
            }
        },
        "status": {
            "title":    "Status",
            "type":     "barStatus",
            "reg":      "MB_Global_Status",
            "flags": {
                "DC OC":    0x10,
                "DC OV":    0x40,
                "Drv Err":  0x20
            }
        },
        "iq": {
            "title":    "Torque/Iq",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Iq",
            "act_reg":  "MB_Act_Iq",
            "unit":     "A",
            "min":      -10,
            "max":       10,
            "decs":       1
        },
        "id": {
            "title":    "Flux/Id",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Id",
            "act_reg":  "MB_Act_Id",
            "unit":     "A",
            "min":      -10,
            "max":       10,
            "decs":       1
        },
        "vel": {
            "title":    "Velocity",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Speed",
            "act_reg":  "MB_Act_Speed",
            "unit":     "RPM",
            "min":      -1800,
            "max":       1800,
            "decs":         0
        },
        "pos": {
            "title":    "Position",
            "type":     "barSetpoint",
            "reg":      "MB_SP_PosMech",
            "act_reg":  "MB_Act_PosMech",
            "unit":     "\N{DEGREE SIGN}",
            "min":      -180,
            "max":       180,
            "decs":        1
        },
        "dc_bus": {
            "title":        "DC Bus",
            "type":         "barDcBus",
            "volt_reg":     "MB_Act_DcBus_U",
            "volt_decs":    0,
            "curr_reg":     "MB_Act_DcBus_I",
            "curr_decs":    1
        },
        "dac_config": {
            "title":        "DACs",
            "type":         "barDac",
            "regs": {
                "DAC 1":    "MB_Dac1Cfg",
                "DAC 2":    "MB_Dac2Cfg",
                "DAC 3":    "MB_Dac3Cfg",
                "DAC 4":    "MB_Dac4Cfg",
            },
            "choose": {
                 0: "pi_iq_Ref",
                 1: "pi_iq_Fdb",
                 2: "pi_iq_Out",
                 3: "pi_id_Ref",
                 4: "pi_id_Fdb",
                 5: "pi_id_Out",
                 6: "pi_spd_Ref",
                 7: "pi_spd_Fdb",
                 8: "pi_spd_Out",
                 9: "pi_pos_Ref",
                10: "pi_pos_Fdb",
                11: "pi_pos_Out",
                12: "pi_iexc_Ref",
                13: "pi_iexc_Fdb",
                14: "pi_iexc_Out",
                15: "i_u",
                16: "i_v",
                17: "i_w",
                18: "theta_mech",
                19: "theta_el",
                20: "dcbus_i",
                21: "dcbus_u",
                22: "TestTorque_angle_offset_sp",
                23: "Ramp"
            }
        }
    }
