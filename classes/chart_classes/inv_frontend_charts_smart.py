import matplotlib.pyplot as plt
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QVBoxLayout, QGroupBox, QComboBox, QHBoxLayout, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib import animation

"""
Kucharczyk Bartlomiej
mPower Sp. z o.o.
01.09.2020

Smart charts - klasa przyjmuje wartosci nazwy adresow jako parametry i wyswietla.
"""

class ChartsRD(QGroupBox):

    def __init__(self, invFront, test, **kwargs):
        super().__init__()

        self.invFront = invFront
        self.test = test
        self.setTitle("Charts")
        self.labels = False

        self.data1 = [0]
        self.data2 = [0]
        self.data1Name = ''
        self.data2Name = ''

        mainLayout = QVBoxLayout()

        # Chart
        chartLayout = QVBoxLayout()
        self.figure = plt.figure()
        self.figure.patch.set_facecolor("#F0F0F0")
        self.canvas = FigureCanvas(self.figure)
        chartLayout.addWidget(self.canvas)

        hbox1 = QHBoxLayout()
        lbl1 = QLabel("Input 1")
        self.data1Select = QComboBox()
        self.data1Select.currentIndexChanged.connect(self.__SignalChanged)

        hbox2 = QHBoxLayout()
        lbl2 = QLabel("Input 2")
        self.data2Select = QComboBox()
        self.data2Select.currentIndexChanged.connect(self.__SignalChanged)

        for key, value in self.invFront.invControl.registerDefs.items():  # accessing keys
            self.data1Select.addItem(str(key))
            self.data2Select.addItem(str(key))

        hbox1.addWidget(lbl1)
        hbox1.addWidget(self.data1Select)

        hbox2.addWidget(lbl2)
        hbox2.addWidget(self.data2Select)

        mainLayout.addLayout(chartLayout)
        mainLayout.addLayout(hbox1)
        mainLayout.addLayout(hbox2)

        if 'testName' in kwargs:

            self.label1 = QLabel(" Act. vlv:" + "0.0")
            self.label2 = QLabel(" Act. vlv:" + "0.0")

            hbox1.addWidget(self.label1)
            hbox2.addWidget(self.label2)
            self.labels = True

        self.setLayout(mainLayout)

        self.ax = self.figure.add_subplot(211)
        self.ax.grid(True)
        self.ay = self.figure.add_subplot(212)
        self.ay.grid(True)
        self.__update(1)

        self.anim1 = animation.FuncAnimation(self.figure, self.__update, frames=720, interval=5000)

        automaticTimer = QTimer(self)
        automaticTimer.timeout.connect(self.__getData)
        automaticTimer.start(self.test.cycleTime)

    def newTestStarted(self):
        self.data1 = [0]
        self.data2 = [0]

    def __getData(self):
        if self.test.step != 0:
            self.data1.append(self.invFront.invControl.getReg(self.data1Name))
            self.data2.append(self.invFront.invControl.getReg(self.data2Name))
        if self.labels:
            self.__setTextLabel(self.data1, self.data2)


    def __update(self, i):
        self.ax.cla()
        self.ay.cla()

        self.ax.plot(self.data1, 'b-')
        self.ax.grid(True)
        self.ax.set(ylabel=self.data1Name, title='Read value')

        self.ay.plot(self.data2, 'b-')
        self.ay.grid(True)
        self.ay.set(xlabel='sample', ylabel=self.data2Name)


    def stopUpdate(self):
        #self.anim1.event_source.
        pass

    def __SignalChanged(self):
        self.data1 = [0]
        self.data2 = [0]

        self.data1Name = self.data1Select.currentText()
        self.data2Name = self.data2Select.currentText()

    def __setTextLabel(self, l1, l2):
        if len(l1) > 0:
            self.label1.setText("    Act. vlv:  " + str(round(l1[len(l1) - 1], 3)))
        if len(l2) > 0:
            self.label2.setText("    Act. vlv:  " + str(round(l2[len(l2) - 1], 3)))