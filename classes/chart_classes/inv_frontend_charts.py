from PyQt5.QtWidgets import QVBoxLayout, QGroupBox, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib import animation

class TestCharts(QVBoxLayout):

    def __init__(self, test):
        super().__init__()
        self.testClass = test

        box = QGroupBox()
        box.setTitle("Charts")
        mainLayout = QVBoxLayout()

        # Labels
        labelLayout = QVBoxLayout()
        self.label1 = QLabel("Angle value set:    0.0")
        self.label2 = QLabel("Current value set: 0.0")
        labelLayout.addWidget(self.label1)
        labelLayout.addWidget(self.label2)

        # Chart
        chartLayout = QVBoxLayout()
        self.figure = plt.figure()
        self.figure.patch.set_facecolor("#F0F0F0")
        self.canvas = FigureCanvas(self.figure)
        chartLayout.addWidget(self.canvas)

        mainLayout.addLayout(chartLayout)
        mainLayout.addLayout(labelLayout)
        box.setLayout(mainLayout)

        self.addWidget(box)

        self.ax = self.figure.add_subplot(211)
        self.ax.grid(True)
        self.ay = self.figure.add_subplot(212)
        self.ay.grid(True)
        self.__update(1)

        self.anim1 = animation.FuncAnimation(self.figure, self.__update, frames=720, interval=5000)

    def __update(self, i):
        self.ax.cla()
        self.ay.cla()

        l1, l2 = self.testClass.getLineData()

        self.ax.plot(l1, 'b-')
        self.ax.grid(True)
        self.ax.set(ylabel='Enc set', title='Set value')

        self.ay.plot(l2, 'b-')
        self.ay.grid(True)
        self.ay.set(xlabel='sample', ylabel='Current set')

        self.__setTextLabel(l1, l2)

    def __updateChart(self):
        self.figure.clear()
        l1, l2 = self.testClass.getLineData()
        self.__drawChart(l1, l2)
        self.canvas.draw()

    def __drawChart(self, l1, l2):
        ax = self.figure.add_subplot(211)
        ay = self.figure.add_subplot(212)

        ax.set(ylabel='Enc set', title='Set value')
        ax.grid()
        ay.set(xlabel='sample', ylabel='Current set')
        ay.grid()
        self.__setTextLabel(l1, l2)
        ax.plot(l1, 'b-')
        ay.plot(l2, 'b-')

    def __setTextLabel(self,l1,l2):
        if len(l1) > 0:
            self.label1.setText("Angle value set:    " + str(round(l1[len(l1)-1], 3)))
        if len(l2) > 0:
            self.label2.setText("Current value set: " + str(round(l2[len(l2)-1], 3)))