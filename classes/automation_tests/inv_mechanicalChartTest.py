from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QWidget

"""
Bartlomiej Kucharczyk
mPower Sp. z o.o.
01.10.2020

Osobna klasa na maszyne stanow automatycznego testu
"""

class MechChartTest(QWidget):
    def __init__(self, invFront, secInvFront):
        super().__init__()

        self.invFront = invFront
        self.secInvFront = secInvFront
        self.step = 0

        self.setCurrent = 0
        self.setVelocity = 0

        self.startAutomaticTest = False
        self.elapsedTime = 0

        self.testTime = 0
        self.cycleTime = 10

        self.currentMin  = 0
        self.currentMax  = 0
        self.currentStep = 0

        self.velMin = 0
        self.velMax = 0
        self.velStep = 0

        self.timeStep = 0
        self.mode = False
        self.testFinished = False

        self.data1 = []
        self.data2 = []

        automaticTimer = QTimer(self)
        automaticTimer.timeout.connect(self.__automaticTest)
        automaticTimer.start(self.cycleTime)

    def startTest(self, currentMin, currentStep, currentMax, velMin, velStep, velMax, timeStep, mode):
        self.currentMin  = currentMin
        self.currentMax  = currentMax
        self.currentStep = currentStep

        self.velMin    = velMin
        self.velStep   = velStep
        self.velMax    = velMax

        self.timeStep    = timeStep
        self.mode        = mode

        self.data1 = []
        self.data2 = []
        self.startAutomaticTest = True

    def stopTest(self):
        self.step = 99

    def __automaticTest(self):
        if self.step > 1:
            self.testTime += self.cycleTime / 1000
            self.data1.append(self.setVelocity)
            self.data2.append(self.setCurrent)
        else:
            self.testTime = 0

        if self.step == 0: # Wait for start
            if self.startAutomaticTest:
                self.startAutomaticTest = False
                self.__turnOnInverters()
                self.step = 1

        elif self.step == 1: # Check the motor is start
            if self.invFront.invControl.getReg("MB_Inverter_State") == 3:
                self.step = 2

        elif self.step == 2: # Start move the second
            self.secInvFront.invControl.setReg("MB_Command", 8)
            self.step = 3


        elif self.step == 3: # Set the first current
            self.setCurrent = self.currentMin
            self.setVelocity = self.velMin
            self.setVelAndCurrent(True, True)
            self.elapsedTime = self.cycleTime
            self.step = 4

        elif self.step == 4: # Wait time
            self.elapsedTime += self.cycleTime

            if self.elapsedTime >= (self.timeStep*1000):
                self.step = 5


        elif self.step == 5: # Increase the current and come back to 3. If encValue >= max setZero
            self.setCurrent += self.currentStep

            if self.setCurrent < self.currentMax:
                self.setVelAndCurrent(False, True)
                self.elapsedTime = self.cycleTime
                self.step = 4
            else:
                self.setCurrent -= self.currentStep
                self.step = 6

        elif self.step == 6: # Increases velocity, decrease current

            if self.setVelocity + self.velStep <= self.velMax:
                self.elapsedTime = self.cycleTime*2

                if self.mode:
                    self.setVelocity += self.velStep
                    self.setCurrent = self.currentMin
                    self.setVelAndCurrent(True, True)
                    self.step = 4

                else: # Decrease the angle set
                    self.setCurrent -= self.currentStep
                    self.setVelAndCurrent(False, True)
                    self.step = 8
            else:
                self.setCurrent = 0
                self.setVelocity = 0
                self.setVelAndCurrent(True, True)
                self.step = 99

        elif self.step == 8: # Decrease delay ramp
            self.elapsedTime += self.cycleTime

            if self.elapsedTime >= (self.timeStep * 1000):
                self.step = 7

        elif self.step == 7: # Decrease ramp
            self.setCurrent -= self.currentStep

            if self.setCurrent >= self.currentMin:
                self.setVelAndCurrent(False, True)
                self.elapsedTime = self.cycleTime
                self.step = 8
            else:
                self.setVelocity += self.velStep
                self.setCurrent = self.currentMin
                self.setVelAndCurrent(True, True)
                self.step = 3



        elif self.step == 99: # end of test
            self.step = 0
            self.elapsedTime = 0

            if self.setVelocity  > 0:
                self.setCurrent = 0
                self.setVelocity = 0

                self.data1.append(self.setVelocity)
                self.data2.append(self.setCurrent)

                self.setVelAndCurrent(True, True)

            self.__turnOffInverters()
            self.testFinished = True

        #self.setRefValue()

    def setVelAndCurrent(self, velocity, current):
        if velocity:
            self.secInvFront.invControl.setReg("MB_SP_Speed", self.setVelocity)

        if current:
            self.invFront.invControl.setReg("MB_SP_Iq", self.setCurrent)

        self.secInvFront.invControl.setReg("MB_IQ_Ref", [int(self.setCurrent), int(self.invFront.invControl.getReg("MB_Act_Iq_Out")), int(self.invFront.invControl.getReg("MB_Act_Id_Out"))])

    def setRefValue(self):
        self.secInvFront.invControl.setReg("MB_IQ_Ref", self.setCurrent)

    def __turnOnInverters(self):
        self.secInvFront.invControl.setReg("MB_Command", 2)
        self.invFront.invControl.setReg("MB_Command", 2)

    def __turnOffInverters(self):
        self.secInvFront.invControl.setReg("MB_Command", 16)
        self.invFront.invControl.setReg("MB_Command", 1)
        self.secInvFront.invControl.setReg("MB_Command", 1)

    def getTestState(self):
        return self.step, self.testTime

    def getLineData(self):
        return self.data1, self.data2

    def getSetValue(self):
        return self.setVelocity, self.setCurrent