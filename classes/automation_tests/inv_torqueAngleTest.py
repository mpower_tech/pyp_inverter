from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QWidget

"""
Bartlomiej Kucharczyk
mPower Sp. z o.o.
31.08.2020

Osobna klasa na maszyne stanow automatycznego testu
"""

class TorqueAngleTest(QWidget):
    def __init__(self, invFront):
        super().__init__()

        self.invFront = invFront
        self.step = 0

        self.setCurrent = 0
        self.setAngle = 0

        self.startAutomaticTest = False
        self.elapsedTime = 0

        self.testTime = 0
        self.cycleTime = 10

        self.currentMin  = 0
        self.currentMax  = 0
        self.currentStep = 0

        self.angleMin = 0
        self.angleMax = 0
        self.angleStep = 0

        self.timeStep = 0
        self.mode = False
        self.testFinished = False

        self.data1 = []
        self.data2 = []

        automaticTimer = QTimer(self)
        automaticTimer.timeout.connect(self.__automaticTest)
        automaticTimer.start(self.cycleTime)

    def startTest(self, currentMin, currentStep, currentMax, angleMin, angleStep, angleMax, timeStep, mode):
        self.__setVarMode()
        self.currentMin  = currentMin
        self.currentMax  = currentMax
        self.currentStep = currentStep

        self.angleMin    = angleMin
        self.angleStep   = angleStep
        self.angleMax    = angleMax

        self.timeStep    = timeStep
        self.mode        = mode

        self.data1 = []
        self.data2 = []
        self.startAutomaticTest = True

    def stopTest(self):
        self.step = 6

    def __automaticTest(self):
        if self.step > 1:
            self.testTime += self.cycleTime / 1000
            self.data1.append(self.setAngle)
            self.data2.append(self.setCurrent)
        else:
            self.testTime = 0

        if self.step == 0: # Wait for start
            if self.startAutomaticTest:
                self.startAutomaticTest = False
                self.__turnOnInverter()
                self.step = 1

        elif self.step == 1: # Check the motor is start
            if self.invFront.invControl.getReg("MB_Inverter_State") == 3:
                self.step = 2

        elif self.step == 2: # Set the first current
            self.setCurrent = self.currentMin
            self.setAngle = self.angleMin
            self.setAngleAndCurrent(True, True)
            self.elapsedTime = self.cycleTime
            self.step = 3

        elif self.step == 3: # Wait time
            self.elapsedTime += self.cycleTime

            if self.elapsedTime >= (self.timeStep*1000):
                self.step = 4


        elif self.step == 4: # Increase the encValue and come back to 3. If encValue >= max setZero
            self.setAngle += self.angleStep

            if self.setAngle < self.angleMax:
                self.setAngleAndCurrent(True, False)
                self.elapsedTime = self.cycleTime
                self.step = 3
            else:
                self.setAngle -= self.angleStep
                self.step = 5

        elif self.step == 5: # Increases current Id, decrease encValue

            if self.setCurrent + self.currentStep < self.currentMax:
                self.elapsedTime = self.cycleTime*2

                if self.mode:
                    self.setCurrent += self.currentStep
                    self.setAngle = self.angleMin
                    self.setAngleAndCurrent(True, True)
                    self.step = 3
                else: # Decrease the angle set
                    self.setAngle -= self.angleStep
                    self.setAngleAndCurrent(True, False)
                    self.step = 8
            else:
                self.setCurrent = 0
                self.setAngle = 0
                self.setAngleAndCurrent(True, True)
                self.step = 6

        elif self.step == 7: # Decrease ramp
            self.setAngle -= self.angleStep

            if self.setAngle >= self.angleMin:
                self.setAngleAndCurrent(True, False)
                self.elapsedTime = self.cycleTime
                self.step = 8
            else:
                self.setCurrent += self.currentStep
                self.setAngle = self.angleMin
                self.setAngleAndCurrent(True, True)
                self.step = 3

        elif self.step == 8: # Decrease delay ramp
            self.elapsedTime += self.cycleTime

            if self.elapsedTime >= (self.timeStep * 1000):
                self.step = 7

        elif self.step == 6: # end of test
            self.step = 0
            self.elapsedTime = 0

            if self.setCurrent > 0:
                self.setCurrent = 0
                self.setAngle = 0
                self.setAngleAndCurrent(True, True)

            self.__turnOffInverter()
            self.testFinished = True


    def setAngleAndCurrent(self, angle, current):
        if angle:
            self.invFront.invControl.setReg("MB_AS_var", self.setAngle)
        if current:
            self.invFront.invControl.setReg("MB_SP_Id", self.setCurrent)

    def __turnOnInverter(self):
        self.invFront.invControl.setReg("MB_Command", 2)

    def __turnOffInverter(self):
        self.invFront.invControl.setReg("MB_Command", 1)

    def __setVarMode(self):
        self.invFront.invControl.setReg("MB_AngleSource", 1)

    def getTestState(self):
        return self.step, self.testTime

    def getLineData(self):
        return self.data1, self.data2