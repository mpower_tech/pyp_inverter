from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QDialog, \
    QVBoxLayout, QGridLayout, QGroupBox, QLabel, QDoubleSpinBox, QPushButton, QProgressBar, QHBoxLayout, QCheckBox
from PyQt5.QtWidgets import QMessageBox
from classes.chart_classes.inv_frontend_charts import TestCharts
from classes.chart_classes.inv_frontend_charts_smart import ChartsRD
from classes.automation_tests.inv_mechanicalChartTest import MechChartTest

class MechChartTestConfig(QDialog):

    def __init__(self, listInvFrontend):
        super().__init__()

        self.setWindowTitle("Mechanical chart test")
        self.numOfModbusRTU = None
        self.invFront       = None
        self.secInvFront    = None
        self.driveOnViaTest = False

        if len(listInvFrontend.values()) < 2:
            QMessageBox.warning(self, "Error", "Two inverter are required to run the test.")
            return

        for i in range(len(listInvFrontend.values())):
            if listInvFrontend[i].title() == "Modbus RTU":
                self.numOfModbusRTU = i
                self.invFront = listInvFrontend[i]
            else:
                self.secInvFront = listInvFrontend[i]

        if self.numOfModbusRTU == None:
            QMessageBox.warning(self, "Error", "Inverter not found.")
            return

        layout = QVBoxLayout()
        layout2 = QVBoxLayout()
        layoutStatus = QVBoxLayout()

        box = QGroupBox()
        box.setTitle("Config")
        boxLayout = QGridLayout()

        # min - step - max
        for i, capt in {1: "min", 2: "step", 3: "max"}.items():
            label = QLabel(capt)
            label.setMinimumWidth(50)
            label.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
            boxLayout.addWidget(label, 0, i)

        # prad
        self.currMin = QDoubleSpinBox()
        self.currMin.setKeyboardTracking(False)
        self.currMin.setAlignment(Qt.AlignRight)
        self.currMin.setRange(0, 1)
        self.currMin.setValue(0.0)
        self.currMin.setSingleStep(0.01)
        self.currMin.valueChanged.connect(self.updateTime)

        self.currMax = QDoubleSpinBox()
        self.currMax.setKeyboardTracking(False)
        self.currMax.setAlignment(Qt.AlignRight)
        self.currMax.setRange(0, 1)
        self.currMax.setValue(0.1)
        self.currMax.setSingleStep(0.01)
        self.currMax.valueChanged.connect(self.updateTime)

        self.currStep = QDoubleSpinBox()
        self.currStep.setKeyboardTracking(False)
        self.currStep.setAlignment(Qt.AlignRight)
        self.currStep.setRange(0, 1)
        self.currStep.setValue(0.01)
        self.currStep.setSingleStep(0.01)
        self.currStep.valueChanged.connect(self.updateTime)

        boxLayout.addWidget(
            QLabel("{} \nIq current ".format(listInvFrontend[self.numOfModbusRTU].title())), 1, 0)

        boxLayout.addWidget(self.currMin, 1, 1)
        boxLayout.addWidget(self.currStep, 1, 2)
        boxLayout.addWidget(self.currMax, 1, 3)

        # Velocity
        self.velMin = QDoubleSpinBox()
        self.velMin.setKeyboardTracking(False)
        self.velMin.setAlignment(Qt.AlignRight)
        self.velMin.setSingleStep(10)
        self.velMin.setRange(0, 200)
        self.velMin.setValue(0)
        self.velMin.valueChanged.connect(self.updateTime)

        self.velMax = QDoubleSpinBox()
        self.velMax.setKeyboardTracking(False)
        self.velMax.setAlignment(Qt.AlignRight)
        self.velMax.setSingleStep(10)
        self.velMax.setRange(0, 1200)
        self.velMax.setValue(300)
        self.velMax.valueChanged.connect(self.updateTime)

        self.velStep = QDoubleSpinBox()
        self.velStep.setKeyboardTracking(False)
        self.velStep.setAlignment(Qt.AlignRight)
        self.velStep.setRange(0, 100)
        self.velStep.setSingleStep(10)
        self.velStep.setValue(10)
        self.velStep.valueChanged.connect(self.updateTime)

        boxLayout.addWidget(
            QLabel("Velocity [RPM]"), 2, 0)

        boxLayout.addWidget(self.velMin, 2, 1)
        boxLayout.addWidget(self.velStep, 2, 2)
        boxLayout.addWidget(self.velMax, 2, 3)

        # time step
        self.timeStep = QDoubleSpinBox()
        self.timeStep.setKeyboardTracking(False)
        self.timeStep.setAlignment(Qt.AlignRight)
        self.timeStep.setRange(0.1, 60.0)
        self.timeStep.setValue(0.5)
        self.timeStep.setSingleStep(0.1)
        self.timeStep.valueChanged.connect(self.updateTime)


        boxLayout.addWidget(QLabel("Time step [s]"), 3, 0)
        boxLayout.addWidget(self.timeStep, 3, 2)

        GroupBoxCheckbox = QGroupBox()
        GroupBoxCheckbox.setTitle("Ramp mode")
        GroupBoxCheckbox.setMaximumHeight(70)

        self.checkBoxZero = QCheckBox("Immediate zero")
        self.checkBoxZero.setAutoExclusive(True)
        self.checkBoxZero.setChecked(True)
        self.checkBoxZero.clicked.connect(self.updateTime)

        self.checkBoxRamp = QCheckBox("Zero by ramp")
        self.checkBoxRamp.setAutoExclusive(True)
        self.checkBoxRamp.clicked.connect(self.updateTime)

        layoutChBox = QVBoxLayout()
        layoutChBox.addWidget(self.checkBoxZero)
        layoutChBox.addWidget(self.checkBoxRamp)
        GroupBoxCheckbox.setLayout(layoutChBox)

        ###
        self.torqueAngleTest = MechChartTest(self.invFront, self.secInvFront)
        ###
        box.setLayout(boxLayout)

        boxBtn = QGroupBox()
        boxBtn.setTitle("Control")

        btnStart = QPushButton("Start")
        btnStart.clicked.connect(self.__startTest)

        btnStop = QPushButton("Stop")
        btnStop.clicked.connect(self.torqueAngleTest.stopTest)

        btnResetError = QPushButton("Error reset")
        btnResetError.clicked.connect(self.__errorReset)

        showHideChart = QPushButton("Show / hide chart")
        showHideChart.clicked.connect(lambda : self.chartRD.setHidden(not self.chartRD.isHidden()) )

        boxStatus = QGroupBox()
        boxStatus.setTitle("Status")

        self.labelTime = QLabel("Estimated test time:")
        self.labelState = QLabel("Test state")

        self.progressBar = QProgressBar()
        self.progressBar.setAlignment(Qt.AlignCenter)
        self.labelElapsedTime = QLabel("Elapsed time: 0 s")

        self.firstSetValue = QLabel("Velocity set: -- RPM")
        self.secondSetValue = QLabel("Current set: -- ")

        layoutStatus.addWidget(self.labelTime)
        layoutStatus.addWidget(self.labelState)
        layoutStatus.addWidget(self.firstSetValue)
        layoutStatus.addWidget(self.secondSetValue)
        layoutStatus.addWidget(self.progressBar)
        layoutStatus.addWidget(self.labelElapsedTime)

        layout2.addWidget(btnStart)
        layout2.addWidget(btnStop)
        layout2.addWidget(btnResetError)
        layout2.addWidget(showHideChart)

        boxBtn.setLayout(layout2)
        boxStatus.setLayout(layoutStatus)
        boxStatus.setMaximumWidth(280)

        layout.addWidget(box)
        layout.addWidget(GroupBoxCheckbox)
        layout.addWidget(boxBtn)
        layout.addWidget(boxStatus)

        tmr = QTimer(self)
        tmr.timeout.connect(self.__updateWidgets)
        tmr.start(200)

        self.testChartLayout = ChartsRD(self.invFront, self.torqueAngleTest, testName=True)
        self.chartRD = ChartsRD(self.secInvFront, self.torqueAngleTest,testName=True)
        mainLayout = QHBoxLayout()
        mainLayout.addLayout(layout)
        mainLayout.addWidget(self.testChartLayout)
        mainLayout.addWidget(self.chartRD)

        self.updateTime()
        self.setLayout(mainLayout)

        if self.exec_() == QDialog.Rejected:
            del self.testChartLayout
            del self.chartRD
            del self.torqueAngleTest
            tmr.stop()

    def __updateWidgets(self):

        if self.invFront.invControl.getReg("MB_Inverter_Error") != 0 or (self.secInvFront.invControl.getReg("MB_Global_Status") & 0x08):
            self.labelState.setText("Test state: ERROR DRIVE")
            if self.driveOnViaTest:
                self.__stopDrives()

        elif self.driveOnViaTest:
            step, testTime = self.torqueAngleTest.getTestState()

            self.progressBar.setValue(testTime)
            self.labelElapsedTime.setText("Elapsed time: " + str(round(testTime)) + " s")

            vlv1,vlv2 = self.torqueAngleTest.getSetValue()
            self.firstSetValue.setText("Velocity set: " + str(vlv1) + " RPM")
            self.secondSetValue.setText("Current set: " + str(round(vlv2, 2)))

            if step == 1:
                self.labelState.setText("Test state: WAITING FOR INVERTER ENABLE")
            else:
                self.labelState.setText("Test state: TEST STARTED")
        else:
            self.progressBar.setValue(0)
            self.labelState.setText("Test state: WAITING FOR START")

        if self.torqueAngleTest.testFinished:
            self.torqueAngleTest.testFinished = False
            self.driveOnViaTest = False
            self.__turnOnOffWidgets(True)

    def updateTime(self):
        try:
            if self.checkBoxZero.isChecked():
                timeEstimated = round(((((self.currMax.value() - self.currMin.value()) / self.currStep.value())+1) * self.timeStep.value()) \
                * ((((self.velMax.value() - self.velMin.value()) / self.velStep.value()))+1))

            else:
                timeEstimated = round(((((self.currMax.value() - self.currMin.value()) / self.currStep.value()) + 1) * self.timeStep.value()) \
                * ((((self.velMax.value() - self.velMin.value()) / self.velStep.value())) + 1)) * 2

            self.progressBar.setRange(0, timeEstimated)
            self.labelTime.setText("Estimated test time: " + str(timeEstimated) + " s")
        except:
            self.progressBar.setRange(0, 0)
            self.labelTime.setText("Estimated test time: INDEFINITE")

    def __startTest(self):
        if self.driveOnViaTest:
            return

        # Sprawdzenie warunku rozpoczecia testu - brak bledu:
        if self.invFront.invControl.getReg("MB_Inverter_Error") != 0 and not bool(self.secInvFront.invControl.getReg("MB_Global_Status") & 0x08):
            self.torqueAngleTest.stopTest()
            return

        # Sprawdzenie zalaczenia napedu
        if self.invFront.invControl.getReg("MB_Inverter_State") == 0: # Jak iddle to zalacz naped
            self.torqueAngleTest.startTest(self.currMin.value(),
                                           self.currStep.value(),
                                           self.currMax.value(),
                                           self.velMin.value(),
                                           self.velStep.value(),
                                           self.velMax.value(),
                                           self.timeStep.value(),
                                           self.checkBoxZero.isChecked())
            self.__turnOnOffWidgets(False)
            self.driveOnViaTest = True
            self.chartRD.newTestStarted()
            self.testChartLayout.newTestStarted()

        else:
            # W przeciwnym razie nie rob nic. Naped moze jechac. Nie mozna uruchomic procedury
            # jak naped jest juz w ruchu. Moze byc inny nadzor / test wlaczony.
            self.torqueAngleTest.stopTest()
            return

    def __errorReset(self):
        self.invFront.invControl.setReg("MB_Command", 4)
        self.secInvFront.invControl.setReg("MB_Command",4)

    def __turnOnOffWidgets(self, state):
        if state:
            self.timeStep.setEnabled(True)
            self.velMin.setEnabled(True)
            self.velStep.setEnabled(True)
            self.velMax.setEnabled(True)
            self.currMin.setEnabled(True)
            self.currMax.setEnabled(True)
            self.currStep.setEnabled(True)
            self.checkBoxRamp.setEnabled(True)
            self.checkBoxZero.setEnabled(True)
        else:
            self.timeStep.setEnabled(False)
            self.velMin.setEnabled(False)
            self.velStep.setEnabled(False)
            self.velMax.setEnabled(False)
            self.currMin.setEnabled(False)
            self.currMax.setEnabled(False)
            self.currStep.setEnabled(False)
            self.checkBoxRamp.setEnabled(False)
            self.checkBoxZero.setEnabled(False)

    def __stopDrives(self):
        self.torqueAngleTest.stopTest()