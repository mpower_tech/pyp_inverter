from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel

# Author: Bartlomiej Kucharczyk
# Date: 10.08.2020
# Class name: InvAcsParams
# Desc: This class create layout for 3 temp value

class InvAcsParams(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()

        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        self.setAlignment(Qt.AlignTop)
        self.setFixedHeight(50)
        self.setMinimumWidth(300)

        self.current = kwargs.get("cur_regs")
        self.freq = kwargs.get("freq_regs")
        self.power = kwargs.get("pwr_regs")

        self.descCurr = kwargs.get("curr_decs")
        self.descFreq = kwargs.get("curr_decs")
        self.descPwr = kwargs.get("curr_decs")

        self.unit = kwargs.get("unit")

        f = QFont("Helvetica")
        f.setPointSize(f.pointSize() + 1)
        f.setBold(True)

        self.valTemp1 = None
        self.valTemp2 = None
        self.valTemp3 = None

        self.lbCurrent = QLabel()
        self.lbCurrent.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbCurrent.setFont(f)

        self.lbFreq = QLabel()
        self.lbFreq.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbFreq.setFont(f)

        self.lbPwr = QLabel()
        self.lbPwr.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbPwr.setFont(f)

        self.__lbUpdate()

        layout = QHBoxLayout()
        layout.addWidget(self.lbCurrent)
        layout.addWidget(self.lbFreq)
        layout.addWidget(self.lbPwr)
        self.setLayout(layout)


    def __lbUpdate(self):
        if self.valTemp1 is not None:
            self.lbCurrent.setText("{val} {unit}".format(
                val=self.valTemp1,
              #  decs=self.descCurr,
                unit = self.unit[0]))
        else:
            self.lbCurrent.setText("-- {unit}".format(unit = self.unit[0]))

        if self.valTemp2 is not None:
            self.lbFreq.setText("{val} {unit}".format( # :.{decs}f
                val=self.valTemp2,
                #decs=self.descFreq,
                unit = self.unit[1]))
        else:
            self.lbFreq.setText("-- {unit}".format(unit = self.unit[1]))

        if self.valTemp3 is not None:
            self.lbPwr.setText("{val} {unit}".format( #:.{decs}f
                val=self.valTemp3,
                #decs=self.descPwr,
                unit = self.unit[2]))
        else:
            self.lbPwr.setText("-- {unit}".format(unit = self.unit[2]))


    def regUpdate(self):
        self.valTemp1 = float(self.invControl.getReg(self.current)) \
                          * pow(10, -self.descCurr)

        self.valTemp2 = float(self.invControl.getReg(self.freq)) \
                        * pow(10, -self.descFreq)

        self.valTemp3 = int(self.invControl.getReg(self.power)) \
                        * pow(10, -self.descPwr)

        self.__lbUpdate()
