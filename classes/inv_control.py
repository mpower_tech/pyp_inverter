import serial.tools.list_ports
import minimalmodbus
import ctypes as ct

class InvControl:
    def __init__(self, **kwargs):
        self.regsArray = [0.0] * 48
        self.goodFramesCount  = 0
        self.badFramesCount   = 0

        self.connected = False

        try:
            self.port = kwargs["port"]
            self.instrument = minimalmodbus.Instrument(self.port, 9)
            self.instrument.serial.baudrate = 9600
            self.instrument.serial.timeout = 0.1
            self.instrument.serial.bytesize = 8
            self.instrument.serial.parity = serial.PARITY_NONE
            self.instrument.serial.stopbits = 1
            self.connected = True
        except:
            self.connected = False
            self.badFramesCount += 1

    def getMbusData(self):
        try:
            valMBus = self.instrument.read_registers(0, 30, 3)

            self.goodFramesCount += 1
        except:
            self.badFramesCount += 1
        try:
            for i in range(len(valMBus)):
                if i == 7 or i == 8:
                    try:
                        self.regsArray[i] = ct.c_int16(valMBus[i]).value/32767.0
                    except:
                        self.regsArray[i] = 0

                elif i == 12 or i == 13 or i == 14 or i == 19 or i == 23 or i == 24:
                    try:
                        self.regsArray[i] = ct.c_int16(valMBus[i]).value/32767.0
                    except:
                        self.regsArray[i] = (valMBus[i])/32767.0
                elif i == 10:
                    try:
                        self.regsArray[i] = ct.c_int16(valMBus[i]).value
                    except:
                        self.regsArray[i] = valMBus[i]
                else:
                    self.regsArray[i] = valMBus[i]
        except:
            pass

    def getReg(self, name):
        if not name in self.registerDefs:
          #  print("get nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        val   = self.regsArray[addr - 4000]

        # print("get reg \"{}\" = {}".format(name, val))
        return val

    def setReg(self, name, val):
        if not name in self.registerDefs:
            print("set nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        self.regsArray[addr - 4000]  = val
        #print("set reg \"{}\" = {}".format(name, val))
        try:
            if name == "MB_SP_Iq":
                self.instrument.write_register(38, val*32767, signed=True)
            elif name == "MB_SP_Id":
                self.instrument.write_register(39, val*32767, signed=True)
            elif name == "MB_SP_Speed":
                self.instrument.write_register(41, val*32767, signed=True)
            elif name == "MB_AS_var":
                self.instrument.write_register(32, val * 32767, signed=True)
            else:
                self.instrument.write_register(addr-4000, int(val), signed=True)
            self.goodFramesCount += 1
        except:
            self.badFramesCount += 1
        return True

    def readStatusRegs(self):
        pass
        #self.goodFramesCount += 1

    def getConnType(self):
            return "port " + self.port + " is open"

    def getGoodFramesCount(self):
        return self.goodFramesCount

    def getBadFramesCount(self):
        return self.badFramesCount

    @staticmethod
    def getPortList():
        # KB - wczytanie dostepnych portow
        ports = serial.tools.list_ports.comports()
        portList = dict()

        for p in range(len(ports)):
            portList[p] = dict(
                port  = ports[p].device,
                title = str(ports[p].device)
            )
        return portList

    registerDefs = {
        "MB_Global_Status":     4000,
        "MB_Inverter_State":    4001,
        "MB_Inverter_Error":    4002,
        "MB_Inverter_Warnings": 4003,
        "MB_HW_Ver":            4004,
        "MB_SW_Ver":            4005,
        "MB_ISR_Ticker":        4006,
        "MB_Act_Iq":            4007,
        "MB_Act_Id":            4008,
        "MB_Act_Freq":          4009,
        "MB_Act_Speed":         4010,
        "MB_Act_PosRaw":        4011,
        "MB_Act_PosMech":       4012,
        "MB_Act_PosElec":       4013,
        "MB_Act_ElThetaPark":   4014,
        "MB_Act_DcBus_U":       4015,
        "MB_Act_DcBus_I":       4016,
        "MB_Act_phUIrms":       4017,
        "MB_Act_phVIrms":       4018,
        "MB_Act_phWIrms":       4019,
        "MB_Act_Temp1":         4020,
        "MB_Act_Temp2":         4021,
        "MB_Act_Temp3":         4022,
        "MB_Act_Iq_Out":        4023,
        "MB_Act_Id_Out":        4024,
        "MB_Poles":             4028,
        "MB_Command":           4030,
        "MB_AngleSource":       4031,
        "MB_AS_var":            4032,
        "MB_AS_RampGen_f":      4033,
        "MB_Dac1Cfg":           4034,
        "MB_Dac2Cfg":           4035,
        "MB_Dac3Cfg":           4036,
        "MB_Dac4Cfg":           4037,
        "MB_SP_Iq":             4038,
        "MB_SP_Id":             4039,
        "MB_SP_Speed":          4041,
        "MB_SP_PosMech":        4042,
        "MB_SP_EncOffset":      4043,
        "MB_InverterMode":      4044,
    }

    registerDefsOffset = {
        "MB_Global_Status":     0,
        "MB_Inverter_State":    0,
        "MB_Inverter_Error":    0,
        "MB_Inverter_Warnings": 0,
        "MB_HW_Ver":            0,
        "MB_SW_Ver":            0,
        "MB_ISR_Ticker":        0,
        "MB_Act_Iq":            0,
        "MB_Act_Id":            0,
        "MB_Act_Freq":          0,
        "MB_Act_Speed":         0,
        "MB_Act_PosRaw":        0,
        "MB_Act_PosMech":       0,
        "MB_Act_PosElec":       0,
        "MB_Act_ElThetaPark":   0,
        "MB_Act_DcBus_U":       0,
        "MB_Act_DcBus_I":       0,
        "MB_Act_phUIrms":       0,
        "MB_Act_phVIrms":       0,
        "MB_Act_phWIrms":       0,
        "MB_Act_Temp1":         0,
        "MB_Act_Temp2":         0,
        "MB_Act_Temp3":         0,
        "MB_Act_Iq_Out":        0,
        "MB_Act_Id_Out":        0,
        "MB_Poles":             0,
        "MB_Command":           0,
        "MB_AngleSource":       0,
        "MB_AS_var":            0,
        "MB_AS_RampGen_f":      0,
        "MB_Dac1Cfg":           0,
        "MB_Dac2Cfg":           0,
        "MB_Dac3Cfg":           0,
        "MB_Dac4Cfg":           0,
        "MB_SP_Iq":             0,
        "MB_SP_Id":             0,
        "MB_SP_Speed":          0,
        "MB_SP_PosMech":        0,
        "MB_SP_EncOffset":      0,
        "MB_InverterMode":      0,
    }

    registerDefsModern = {
        "MB_Global_Status": {
            "address":     4000,
            "unit":        "",
            "resolution":  1,
            "offset":      0,
            "maxValue":    32768,
            "minValue":    0
        },
        "MB_Inverter_State": {
            "address": 4001,
            "unit": "",
            "resolution": 1,
            "offset": 0,
            "maxValue": 32768,
            "minValue": 0
        },
    }
    
    frontendCaps = {
        "command": {
            "title":    "Command",
            "type":     "barCommand",
            "reg":      "MB_Command",
            "btn_grp": {
                "Power": {
                    "Enable":   0x02,
                    "Disable":  0x01
                },
                "Reset": {
                    "Reset Errors": 0x04
                }
            }
        },
        "config": {
            "title":    "Config",
            "type":     "barConfig",
            "configs": {
                "Mode": {
                    "reg":      "MB_InverterMode",
                    "choose": {
                        0: "Torque",
                        1: "Speed",
                        2: "Position"
                    },
                    "readonly": True
                },
                "Poles   ": {
                    "reg":      "MB_Poles",
                    "readonly": True
                },
                "Enc Offset": {
                    "reg":      "MB_SP_EncOffset",
                    "min":         0,
                    "max":      1024,
                    "readonly": True
                }
            }
        },
        "encSet": {
            "title": "Encoder options",
            "type": "barEncoSet",
            "configs": {
                "Mode": {
                    "reg": "MB_AngleSource",
                    "choose": {
                        0: "Qep",
                        1: "Var",
                        2: "RampGen"
                    },
                    "readonly": False
                },
                "AS_var    ": {
                    "reg": "MB_AS_var",
                    "min": 0,
                    "max": 1,
                    "readonly": False
                },
                "AS_RampGen_f": {
                    "reg": "MB_AS_RampGen_f",
                    "min": 0,
                    "max": 1000,
                    "readonly": False
                }


            }
        },
        "status": {
            "title":    "Status",
            "type":     "barStatus",
            "reg":      "MB_Inverter_State",
            "flags": {
                "Status":   0x01,
                "Drv Err":  0x20,
                "DC OC":    0x10,
                "DC OV":    0x40
            }
        },
        "err_bus": {
            "title": "Errors",
            "type": "barErrors",
            "reg": "MB_Inverter_Error",
            "flags": {
                "OverCurrentTrip_IL11": 0x01,
                "OverCurrentTrip_IL21": 0x02,
                "OverCurrentTrip_IL31": 0x04,
                "OverVoltageDCTrip1"  : 0x08,
                "DriverTrip_11"       : 0x10,
                "DriverTrip_21"       : 0x20,
                "DriverTrip_31"       : 0x40,
                "CommErrorTrip1"      : 0x80,
                "TempTrip_11"         : 0x100,
                "TempTrip_21"         : 0x200,
                "TempTrip_31"         : 0x400,
                "EStop"               : 0x800
            }
        },
        "warr_bus": {
            "title": "Warnings",
            "type": "barWarnings",
            "reg": "MB_Inverter_Warnings",
            "flags": {
                "EncNoldx:1": 0x01,
                "LowUdc": 0x02,
            }
        },
        "iq": {
            "title":    "Torque/Iq",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Iq",
            "act_reg":  "MB_Act_Iq",
            "unit":     "", #A
            "min":      -1,
            "max":       1,
            "decs":      3
        },
        "id": {
            "title":    "Flux/Id",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Id",
            "act_reg":  "MB_Act_Id",
            "unit":     "", #A
            "min":      -1,
            "max":       1,
            "decs":      3
        },
        "vel": {
            "title":    "Velocity",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Speed",
            "act_reg":  "MB_Act_Speed",
            "unit":     "RPM",
            "min":      -1800,
            "max":       1800,
            "decs":         0,
            "step":         1,
        },
        "pos": {
            "title":    "Position",
            "type":     "barSetpoint",
            "reg":      "MB_SP_PosMech",
            "act_reg":  "MB_Act_PosMech",
            "unit":     "\N{DEGREE SIGN}",
            "min":      -180,
            "max":       180,
            "decs":        1,
            "step":        1,
        },
        "iqid_bus": {
            "title": "Iq / Id out",
            "type": "barIqIdOut",
            "iq_reg": "MB_Act_Iq_Out",
            "iq_decs": 3,
            "id_reg": "MB_Act_Id_Out",
            "id_decs": 3,
            "unit": ["", ""]
        },
        "dc_bus": {
            "title":        "DC Bus",
            "type":         "barDcBus",
            "volt_reg":     "MB_Act_DcBus_U",
            "volt_decs":    0,
            "curr_reg":     "MB_Act_DcBus_I",
            "curr_decs":    1,
            "unit":         ["V","A","W"]
        },
        "temp_bus": {
            "title": "Temperature",
            "type": "barTemp",
            "unit": "\N{DEGREE SIGN}C",
            "temp1_regs": "MB_Act_Temp1",
            "temp1_decs": 1,
            "temp2_regs": "MB_Act_Temp2",
            "temp2_decs": 1,
            "temp3_regs": "MB_Act_Temp3",
            "temp3_decs": 1
            },
        "enc_bus": {
            "title": "Encoder",
            "type": "barEnc",
            "enc_QepRaw": "MB_Act_PosRaw",
            "enc_QepRaw_decs": 0,
            "enc_PosMech": "MB_Act_PosMech",
            "enc_PosMech_decs": 3,
            "enc_PosElec": "MB_Act_PosElec",
            "enc_PosElec_decs": 3,
            "enc_ElThetaPark": "MB_Act_ElThetaPark",
            "enc_ElThetaPark_decs": 3
        },
        "irms_bus": {
            "title": "Irms",
            "type": "barIrms",
            "unit": "pu",
            "phU_Irms": "MB_Act_phUIrms",
            "phU_Irms_decs": 2,
            "phV_Irms": "MB_Act_phVIrms",
            "phV_Irms_decs": 2,
            "phW_Irms": "MB_Act_phWIrms",
            "phW_Irms_decs": 2
        },
        "dac_config": {
            "title":        "DACs",
            "type":         "barDac",
            "regs": {
                "DAC 1":    "MB_Dac1Cfg",
                "DAC 2":    "MB_Dac2Cfg",
                "DAC 3":    "MB_Dac3Cfg",
                "DAC 4":    "MB_Dac4Cfg",
            },
            "choose": {
                 0: "None",
                 1: "pi_iq_Ref",
                 2: "pi_iq_Fdb",
                 3: "pi_iq_Out",
                 4: "pi_id_Ref",
                 5: "pi_id_Fdb",
                 6: "pi_id_Out",
                 7: "pi_spd_Ref",
                 8: "pi_spd_Fdb",
                 9: "pi_spd_Out",
                10: "i_u",
                11: "i_v",
                12: "i_w",
                13: "e_dac_enc_mech",
                14: "e_dac_enc_el",
                15: "e_dac_theta_park",
                16: "dcbus_i",
                17: "dcbus_u",
            }
        }
    }
