import collections

from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QLabel, QVBoxLayout

#
# InvConnStatusBar -- komponent status połączenia
#

class InvConnStatusBar(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()
        
        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        #self.setAlignment(Qt.AlignTop)

        layout = QVBoxLayout()

        self.labelList  = collections.OrderedDict()
        
        for key, title in ({
                "conn_type":    "Connection:",
                "frames_good":  "Good frames:",
                "frames_bad":   "Bad frames:" }).items():
            
            if key != "frames_bad":
                boxLayout = QHBoxLayout()
            
            boxLayout.addWidget(QLabel(title), 1)
            
            label     = QLabel("-")
            label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)
            boxLayout.addWidget(label, 1)
            
            if key == "frames_good":
                boxLayout.addStretch(stretch=1)
            else:
                layout.addLayout(boxLayout)
            
            self.labelList[key] = label
        
        self.setLayout(layout)
        
    def regUpdate(self):
        for key, label in self.labelList.items():
            func  = {
                "conn_type":    self.invControl.getConnType,
                "frames_good":  self.invControl.getGoodFramesCount,
                "frames_bad":   self.invControl.getBadFramesCount
            }.get(key)
            
            label.setText("{}".format(func()))

