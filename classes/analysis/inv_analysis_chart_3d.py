from PyQt5.QtWidgets import QVBoxLayout, QMessageBox
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

class AnalysisChart3DPlot(QVBoxLayout):

    def __init__(self):
        super().__init__()
        mainLayout = QVBoxLayout()

        # Chart
        chartLayout = QVBoxLayout()
        self.figure = plt.figure()
        self.figure.patch.set_facecolor("#F0F0F0")
        self.canvas = FigureCanvas(self.figure)
        self.colorBarExist = False
        self.toolbar = NavigationToolbar(self.canvas, parent=None)
        mainLayout.addWidget(self.toolbar)

        chartLayout.addWidget(self.canvas)
        mainLayout.addLayout(chartLayout)
        self.addLayout(mainLayout)

        self.ax = self.figure.add_subplot()
        self.ax.grid(True)


    def drawChart(self, xVlv, yVlv, zVlv ,xlbl, ylbl, zlbl, id = None):
        #xi = np.linspace(np.min(xVlv), np.max(xVlv), 200)
        #yi = np.linspace(np.min(yVlv), np.max(yVlv), 200)

        #triang = tri.Triangulation(xVlv, yVlv)
        #interpolator = tri.LinearTriInterpolator(triang, zVlv)

        #Xi, Yi = np.meshgrid(xi, yi)
        #zi = interpolator(Xi, Yi)

        self.ax.cla()
        try:
            self.ax.tricontour(xVlv, yVlv, zVlv, linewidths=0.5, colors='k') # ,levels=14
            cntr2 = self.ax.tricontourf(xVlv, yVlv, zVlv, cmap="RdBu_r") # , levels=14

        except:
            QMessageBox.warning(self, "Error","Something goes wrong....")
            return

        if not self.colorBarExist:
            self.colorBarExist = True
            self.figure.colorbar(cntr2, ax=self.ax)

        self.ax.plot(xVlv, yVlv, 'ko', ms=3)
        self.ax.set(xlim=(0, 350), ylim=(0, 5))
        self.ax.grid(True)
        self.ax.set_title('x - predkosc, y - moment, z - napiecie')

        self.canvas.draw()