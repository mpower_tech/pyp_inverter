from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QSplitter, QFrame
from classes.analysis.inv_analysis_read_file import AnalysisReadFile
from classes.analysis.inv_analysis_sheet import Sheet
from classes.analysis.inv_analysis_hide_col import AnalysisHideCol
from classes.analysis.inv_analysis_chart_settings import AnalysisChartSettings
from classes.analysis.inv_analysis_chart_common import AnalysisChartCommon
from PyQt5.QtCore import Qt

class InvAnalysis(QDialog):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Analysis")

        sheetPanel = Sheet()
        hideColumn = AnalysisHideCol(sheetPanel)
        chartCommon = AnalysisChartCommon()

        chartSettings = AnalysisChartSettings(sheetPanel=sheetPanel, chartPlot=chartCommon)
        readFileObj = AnalysisReadFile(sheetPanel, hideColumn, chartSettings)

        layoutLeft = QVBoxLayout()
        layoutLeft.addWidget(readFileObj)
        layoutLeft.addWidget(sheetPanel)
        layoutLeft.addWidget(hideColumn)

        layoutMain = QHBoxLayout()
        left = QFrame()
        left.setLayout(layoutLeft)

        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(left)
        splitter.addWidget(chartCommon)
        splitter.addWidget(chartSettings)

        layoutMain.addWidget(splitter)

        self.setLayout(layoutMain)
        self.exec_()