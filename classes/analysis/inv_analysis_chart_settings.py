from PyQt5.QtWidgets import QTabWidget, QWidget, QFormLayout
from classes.analysis.tabChartSettings.inv_analysis_chart_sett_tab1 import AnalysisTab
from classes.analysis.tabChartSettings.inv_analysis_chart_sett_tab3D import AnalysisTab3D

class AnalysisChartSettings(QTabWidget):
    def __init__(self, parent = None, sheetPanel = None, chartPlot = None):
        super(AnalysisChartSettings, self).__init__(parent)

        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tab4 = QWidget()

        self.addTab(self.tab1, "Tab 1")
        self.addTab(self.tab2, "Tab 2")
        self.addTab(self.tab3, "Tab 3")
        self.addTab(self.tab4, "Tab 3")

        self.setTabText(0, "Common setting")
        layout = QFormLayout()
        self.tab1.setLayout(layout)

        self.setTabText(1, "Chart 1")
        self.analysisTab1 = AnalysisTab(sheetPanel, chartPlot, 1)
        self.tab2.setLayout(self.analysisTab1)

        self.setTabText(2, "Chart 2")
        self.analysisTab2 = AnalysisTab(sheetPanel, chartPlot, 2)
        self.tab3.setLayout(self.analysisTab2)

        self.setTabText(3, "Chart 3D")
        self.analysisTab3D = AnalysisTab3D(sheetPanel, chartPlot)
        self.tab4.setLayout(self.analysisTab3D)

        self.setMaximumWidth(300)

    def updateData(self):
        self.analysisTab1.update()
        self.analysisTab2.update()
        self.analysisTab3D.update()