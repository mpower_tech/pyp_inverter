from PyQt5.QtWidgets import QVBoxLayout, QMessageBox, QPushButton, QHBoxLayout, QButtonGroup
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

class AnalysisChartPlot(QVBoxLayout):

    def __init__(self):
        super().__init__()
        mainLayout = QVBoxLayout()

        # Chart
        chartLayout = QVBoxLayout()
        self.figure = plt.figure()
        self.figure.patch.set_facecolor("#F0F0F0")
        self.canvas = FigureCanvas(self.figure)

        self.toolbar = NavigationToolbar(self.canvas, parent=None)

        btnLayout = QHBoxLayout()
        btnLayout.addWidget(self.toolbar)

        self.btnGroup = QButtonGroup()
        self.btnGroup.buttonClicked[int].connect(self.on_button_clicked)

        for i in range(0, 3):
            btnHide = QPushButton("Plot"+str(i+1))
            self.btnGroup.addButton(btnHide, i+1)
            btnLayout.addWidget(btnHide)

        mainLayout.addLayout(btnLayout)
        chartLayout.addWidget(self.canvas)
        mainLayout.addLayout(chartLayout)
        self.addLayout(mainLayout)

        self.ax = self.figure.add_subplot(311)
        self.ax.grid(True)
        self.ay = self.figure.add_subplot(312)
        self.ay.grid(True)
        self.az = self.figure.add_subplot(313)
        self.az.grid(True)

    def drawChart(self, xVlv, yVlv, xlbl, ylbl, id):
        if id == 1:
            self.ax.cla()

            try:
                self.ax.plot(xVlv, yVlv, 'b-')
            except:
                self.ax.plot(0, 0, 'b-')

            self.ax.grid(True)
            self.ax.set(ylabel=ylbl, title="Plot", xlabel=xlbl)

        if id == 2:
            self.ay.cla()

            try:
                self.ay.plot(xVlv, yVlv, 'b-')
            except:
                self.ay.plot(0, 0, 'b-')

            self.ay.grid(True)
            self.ay.set(ylabel=ylbl, title="", xlabel=xlbl)

        if id == 3:
            self.az.cla()

            try:
                self.az.plot(xVlv, yVlv, 'b-')
            except:
                self.az.plot(0, 0, 'b-')

            self.az.grid(True)
            self.az.set(ylabel=ylbl, title="", xlabel=xlbl)

        self.canvas.draw()

    def on_button_clicked(self, id):
        for button in self.btnGroup.buttons():
            if button is self.btnGroup.button(id):
                if button.text() == 'Plot1':
                    print(self.ax.visible())
                   # self.ax.set_visible(False)
                elif button.text() == 'Plot2':
                    pass
                elif button.text() == 'Plot3':
                    pass