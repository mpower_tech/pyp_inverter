from PyQt5.QtWidgets import QCheckBox, QHBoxLayout, QVBoxLayout, \
    QLabel, QDoubleSpinBox, QAbstractSpinBox
from PyQt5.QtCore import Qt

class AnalysisTabMathMinMax(QVBoxLayout):
    def __init__(self, title):
        super().__init__()

        self.enableCheckBox = QCheckBox(title) # "Enable / disable Y axis"
        self.enableCheckBox.clicked.connect(self.activateMath)

        highVlv = QHBoxLayout()
        lbl1 = QLabel("Maximum value")

        self.valueHigh = QDoubleSpinBox()
        self.valueHigh.setAlignment(Qt.AlignTop)
        self.valueHigh.setMaximumWidth(100)
        self.valueHigh.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.valueHigh.setMaximum(999999)
        self.valueHigh.setMinimum(-999999)
        self.valueHigh.setEnabled(False)

        highVlv.addWidget(lbl1)
        highVlv.addWidget(self.valueHigh)

        lowVlv = QHBoxLayout()
        lbl2 = QLabel("Minimum value")

        self.valueLow = QDoubleSpinBox()
        self.valueLow.setAlignment(Qt.AlignTop)
        self.valueLow.setMaximumWidth(100)
        self.valueLow.setMaximum(999999)
        self.valueLow.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.valueLow.setMinimum(-999999)
        self.valueLow.setEnabled(False)

        lowVlv.addWidget(lbl2)
        lowVlv.addWidget(self.valueLow)

        self.addWidget(self.enableCheckBox)
        self.addLayout(lowVlv)
        self.addLayout(highVlv)
 
    def activateMath(self):
        self.valueLow.setEnabled(self.enableCheckBox.isChecked())
        self.valueHigh.setEnabled(self.enableCheckBox.isChecked())

    def getState(self):
        return self.enableCheckBox.isChecked()

    def getValue(self):
        return self.valueLow.value(), self.valueHigh.value()