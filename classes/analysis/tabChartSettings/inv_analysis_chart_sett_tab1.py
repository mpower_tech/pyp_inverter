from PyQt5.QtWidgets import QCheckBox, QHBoxLayout, QComboBox, QVBoxLayout, \
    QPushButton, QLabel
from PyQt5.QtCore import Qt
from classes.analysis.tabChartSettings.inv_analysis_chart_sett_tab_math import AnalysisTabMath
import numpy
import numpy as np
import csv

class AnalysisTab(QVBoxLayout):
    def __init__(self, sheetPanel = None, chartPlot = None, id = 0):
        super().__init__()

        self.sheetPanel = sheetPanel
        self.chartPlot = chartPlot
        self.id = id
        self.setAlignment(Qt.AlignTop)

        enableCheckBox = QCheckBox("Enable / disable")
        enableCheckBox.setChecked(True)
        enableCheckBox.clicked.connect(self.enableChart)

        comboBoxLayout1 = QHBoxLayout()
        lbl1 = QLabel("X Axis")
        self.xAxis = QComboBox()

        comboBoxLayout1.addWidget(lbl1)
        comboBoxLayout1.addWidget(self.xAxis)

        comboBoxLayout2 = QHBoxLayout()
        lbl2 = QLabel("Y Axis")
        self.yAxis = QComboBox()

        comboBoxLayout2.addWidget(lbl2)
        comboBoxLayout2.addWidget(self.yAxis)

        drawBtn = QPushButton("Plot!")
        drawBtn.clicked.connect(self.btnClicked)

        self.mathBox = AnalysisTabMath(sheetPanel)

        self.addWidget(enableCheckBox)
        self.addLayout(comboBoxLayout1)
        self.addLayout(comboBoxLayout2)
        self.addWidget(drawBtn)
        self.addLayout(self.mathBox)

    def update(self):
        names = self.sheetPanel.getNamesOfColums()

        if self.xAxis.count() > 0:
            self.xAxis.clear()
            self.yAxis.clear()

        self.xAxis.addItems(names)
        self.yAxis.addItems(names)
        self.mathBox.updates()

    def btnClicked(self):
        xVlv, yVlv = self.sheetPanel.getColumnValues(self.xAxis.currentIndex(), self.yAxis.currentIndex())
        # [x,y,advMath, NumOfDelSample, uqindex, udindex]
        math = self.mathBox.getState()
        values = self.mathBox.getValue()


        if int(values[3]) > 0:
            del xVlv[0:int(values[3])]
            del yVlv[0:int(values[3])]

        if math[2]:

            refValue = self.sheetPanel.getColumnValue(values[2][0])
            dx = self.sheetPanel.getColumnValue(values[2][2])

            uqVlv    = self.sheetPanel.getColumnValue(values[4][0])
            udVlv    = self.sheetPanel.getColumnValue(values[4][1])
            uVector  = []

            # Ud Uq
            for x in range(len(uqVlv)):
                uVector.append(numpy.sqrt((uqVlv[x] ** 2) + (udVlv[x] ** 2)))

            # Del num of samles
            if int(values[3]) > 0:
                del refValue[0:int(values[3])]
                del dx[0:int(values[3])]
                del uVector[0:int(values[3])]
            # Gradient
            dy = numpy.gradient(refValue, dx)
            dy = numpy.abs(dy)

            self.chartPlot.drawChart(dx, dy, self.xAxis.currentText(), "Dy/dt", 2)

            windowLen = int(values[2][1])
            windowLenRight = int(values[2][3])

            for x in range(len(dy)):
                if dy[x] >= 2: # 2 - minimalna wartosc pochodnej do wykasowania
                    # Kasuj / None probki z wektora X i Y wartosci
                    #print(x)
                    for y in range(x-windowLen, x+windowLenRight): #2,1 - window length
                        #del xVlv[x-10:x+10] #= None
                        #del yVlv[x-10:x+10] #= None
                        xVlv[y] = None
                        yVlv[y] = None
                        uVector[y] = None

            # Usrednianie
            sumVlv = 0
            sumOfuVectors = 0
            numOfElements = 0
            xVlvConv = []
            yVlvConv = []
            uVectorConv = []
            iterat = 0

            for x in yVlv:
                if x is not None:
                    sumVlv = sumVlv + x
                    numOfElements = numOfElements + 1
                    sumOfuVectors = sumOfuVectors + uVector[iterat]
                else:
                    try:

                        yVlvConv.append(sumVlv/numOfElements)
                        xVlvConv.append(xVlv[iterat-windowLen+2])
                        uVectorConv.append(sumOfuVectors/numOfElements)

                        #print(str(sumVlv/numOfElements))
                    except:
                        pass
                    sumVlv = 0
                    numOfElements = 0
                    sumOfuVectors = 0

                iterat = iterat + 1


            np.savetxt('20.10_KUB.csv', [p for p in zip(xVlvConv, yVlvConv, uVectorConv)], header='Vel; Tqr; Vol', delimiter=';', fmt='%s')

            self.chartPlot.drawChart3D(xVlvConv, yVlvConv, uVectorConv, self.xAxis.currentText(), self.yAxis.currentText(), "1", 1)
            self.chartPlot.drawChart(xVlvConv, yVlvConv, self.xAxis.currentText(), self.yAxis.currentText(), self.id)
            self.chartPlot.drawChart(dx, uVector, "", "", 3)

            #print("Len ref : " + str(len(uVectorConv)))
            #print("Len x   : " + str(len(xVlvConv)))
            #print("Len y   : " + str(len(yVlvConv)))

        else:
            if math[0]:
                for x in range(len(yVlv)):
                    if yVlv[x] > values[0][1] or yVlv[x] < values[0][0]:
                        yVlv[x] = None
                        xVlv[x] = None

            if math[1]:
                for x in range(len(xVlv)):
                    if xVlv[x] == None:
                       continue

                    if xVlv[x] > values[1][1] or xVlv[x] < values[1][0]:
                        yVlv[x] = None
                        xVlv[x] = None

            self.chartPlot.drawChart(xVlv, yVlv, self.xAxis.currentText(), self.yAxis.currentText(), self.id)

        # print("Len x   : " + str(len(xVlv)))
        # print("Len y   : " + str(len(yVlv)))

    def enableChart(self):
        pass
