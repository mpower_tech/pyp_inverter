from PyQt5.QtWidgets import QCheckBox, QHBoxLayout, QVBoxLayout, \
    QLabel, QDoubleSpinBox, QAbstractSpinBox, QComboBox
from PyQt5.QtCore import Qt


class AnalysisTabMathAdv(QVBoxLayout):
    def __init__(self, sheet):
        super().__init__()

        self.sheet = sheet

        self.enableCheckBox = QCheckBox("Advanced analysis")  # "Enable / disable Y axis"
        self.enableCheckBox.clicked.connect(self.enableAdvMath)

        # REFERENCE SINGAL
        comboBoxLayout1 = QHBoxLayout()
        self.qCmbBoxRefSignal = QComboBox()
        self.qCmbBoxRefSignal.setEnabled(False)
        lbl1 = QLabel("Reference signal")

        comboBoxLayout1.addWidget(lbl1)
        comboBoxLayout1.addWidget(self.qCmbBoxRefSignal)

        # DX
        comboBoxLayout3 = QHBoxLayout()
        self.dxderiv = QComboBox()
        self.dxderiv.setEnabled(False)
        lbl3 = QLabel("dx")

        comboBoxLayout3.addWidget(lbl3)
        comboBoxLayout3.addWidget(self.dxderiv)

        # NUM OF SAMPLES TO DELETE + / -
        comboBoxLayout2 = QHBoxLayout()
        lbl2 = QLabel("Window len l [samples]")

        self.windowsLen = QDoubleSpinBox()
        self.windowsLen.setAlignment(Qt.AlignTop)
        self.windowsLen.setMaximumWidth(100)
        self.windowsLen.setMaximum(999999)
        self.windowsLen.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.windowsLen.setMinimum(-999999)
        self.windowsLen.setEnabled(False)

        comboBoxLayout2.addWidget(lbl2)
        comboBoxLayout2.addWidget(self.windowsLen)

        comboBoxLayout4 = QHBoxLayout()
        lbl3 = QLabel("Window len r [samples]")

        self.windowsLenRight = QDoubleSpinBox()
        self.windowsLenRight.setAlignment(Qt.AlignTop)
        self.windowsLenRight.setMaximumWidth(100)
        self.windowsLenRight.setMaximum(999999)
        self.windowsLenRight.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.windowsLenRight.setMinimum(-999999)
        self.windowsLenRight.setEnabled(False)

        comboBoxLayout4.addWidget(lbl3)
        comboBoxLayout4.addWidget(self.windowsLenRight)

        self.addWidget(self.enableCheckBox)
        self.addLayout(comboBoxLayout3)
        self.addLayout(comboBoxLayout1)
        self.addLayout(comboBoxLayout2)
        self.addLayout(comboBoxLayout4)


    def enableAdvMath(self):
        try:
            self.windowsLen.setEnabled(self.enableCheckBox.isChecked())
            self.windowsLenRight.setEnabled(self.enableCheckBox.isChecked())
            self.dxderiv.setEnabled(self.enableCheckBox.isChecked())
            self.qCmbBoxRefSignal.setEnabled(self.enableCheckBox.isChecked())
        except:
            pass

    def getState(self):
        return self.enableCheckBox.isChecked()

    def getValue(self):
        return [self.qCmbBoxRefSignal.currentIndex(), self.windowsLen.value(), self.dxderiv.currentIndex(), self.windowsLenRight.value()]

    def updates(self):
        if self.qCmbBoxRefSignal.count()>0:
            self.qCmbBoxRefSignal.clear()
            self.dxderiv.clear()

        self.qCmbBoxRefSignal.addItems(self.sheet.getNamesOfColums())
        self.dxderiv.addItems(self.sheet.getNamesOfColums())