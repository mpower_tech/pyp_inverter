from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QDoubleSpinBox, QAbstractSpinBox

class AnalysisTabDeleteSample(QHBoxLayout):
    def __init__(self):
        super().__init__()

        lbl1 = QLabel("Delete sample")

        self.valueSampleToDelete = QDoubleSpinBox()
        self.valueSampleToDelete.setAlignment(Qt.AlignTop)
        self.valueSampleToDelete.setMaximumWidth(100)
        self.valueSampleToDelete.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.valueSampleToDelete.setMaximum(999999)
        self.valueSampleToDelete.setMinimum(0)
        self.valueSampleToDelete.setSingleStep(1)
        self.valueSampleToDelete.setDecimals(0)


        self.addWidget(lbl1)
        self.addWidget(self.valueSampleToDelete)

    def getValue(self):
        return self.valueSampleToDelete.value()