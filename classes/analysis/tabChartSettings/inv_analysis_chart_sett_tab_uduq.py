from PyQt5.QtWidgets import QComboBox, QGridLayout, QLabel

class AnalysisTabMathUqUd(QGridLayout):
    def __init__(self, sheet):
        super().__init__()

        self.sheet = sheet

        lbl1 = QLabel("Ud axis")
        self.addWidget(lbl1, 0, 0)

        self.udQCmb = QComboBox()
        self.addWidget(self.udQCmb, 0, 1)

        lbl2 = QLabel("Uq axis")
        self.addWidget(lbl2, 1, 0)

        self.uqQCmb = QComboBox()
        self.addWidget(self.uqQCmb, 1, 1)

    def update(self):
        self.udQCmb.addItems(self.sheet.getNamesOfColums())
        self.uqQCmb.addItems(self.sheet.getNamesOfColums())


    def getValue(self):
        return [self.udQCmb.currentIndex(), self.uqQCmb.currentIndex()]