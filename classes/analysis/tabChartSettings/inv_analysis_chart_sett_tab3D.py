from PyQt5.QtWidgets import QCheckBox, QHBoxLayout, QComboBox, QVBoxLayout, \
    QPushButton, QLabel
from PyQt5.QtCore import Qt

class AnalysisTab3D(QVBoxLayout):
    def __init__(self, sheetPanel = None, chartPlot = None):
        super().__init__()

        self.setAlignment(Qt.AlignTop)
        self.sheetPanel = sheetPanel

        enableCheckBox = QCheckBox("Enable / disable")
        enableCheckBox.setChecked(True)

        comboBoxLayout1 = QHBoxLayout()
        lbl1 = QLabel("X Axis")
        self.xAxis = QComboBox()

        comboBoxLayout1.addWidget(lbl1)
        comboBoxLayout1.addWidget(self.xAxis)

        comboBoxLayout2 = QHBoxLayout()
        lbl2 = QLabel("Y Axis")
        self.yAxis = QComboBox()

        comboBoxLayout2.addWidget(lbl2)
        comboBoxLayout2.addWidget(self.yAxis)

        comboBoxLayout3 = QHBoxLayout()
        lbl3 = QLabel("Z Axis")
        self.zAxis = QComboBox()

        comboBoxLayout3.addWidget(lbl3)
        comboBoxLayout3.addWidget(self.zAxis)

        drawBtn = QPushButton("Plot!")

        self.addWidget(enableCheckBox)
        self.addLayout(comboBoxLayout1)
        self.addLayout(comboBoxLayout2)
        self.addLayout(comboBoxLayout3)
        self.addWidget(drawBtn)


    def update(self):
        names = self.sheetPanel.getNamesOfColums()

        if self.xAxis.count() > 0:
            self.xAxis.clear()
            self.yAxis.clear()
            self.zAxis.clear()

        self.xAxis.addItems(names)
        self.yAxis.addItems(names)
        self.zAxis.addItems(names)



