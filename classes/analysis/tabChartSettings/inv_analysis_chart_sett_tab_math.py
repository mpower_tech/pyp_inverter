from classes.analysis.tabChartSettings.inv_analysis_chart_sett_tab_math_adv import AnalysisTabMathAdv
from classes.analysis.tabChartSettings.inv_analysis_chart_sett_tab_minmax import AnalysisTabMathMinMax
from classes.analysis.tabChartSettings.inv_analysis_chart__sett_del_sample import AnalysisTabDeleteSample
from classes.analysis.tabChartSettings.inv_analysis_chart_sett_tab_uduq import AnalysisTabMathUqUd
from PyQt5.QtWidgets import QVBoxLayout

class AnalysisTabMath(QVBoxLayout):
    def __init__(self, sheet):
        super().__init__()

        self.deleteSamples = AnalysisTabDeleteSample()
        self.minMaxY = AnalysisTabMathMinMax(title='Enable / disable Y axis')
        self.minMaxX = AnalysisTabMathMinMax(title='Enable / disable X axis')
        self.adv = AnalysisTabMathAdv(sheet)
        self.udUq = AnalysisTabMathUqUd(sheet)

        self.addLayout(self.deleteSamples)
        self.addLayout(self.minMaxY)
        self.addLayout(self.minMaxX)
        self.addLayout(self.adv)
        self.addLayout(self.udUq)


    def getValue(self):
        return [self.minMaxY.getValue(), self.minMaxX.getValue(), self.adv.getValue(), self.deleteSamples.getValue(), self.udUq.getValue()]

    def getState(self):
        return [self.minMaxY.getState(), self.minMaxX.getState(), self.adv.getState()]

    def updates(self):
        self.adv.updates()
        self.udUq.update()