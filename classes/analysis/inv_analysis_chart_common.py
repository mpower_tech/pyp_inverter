from PyQt5.QtWidgets import QTabWidget, QWidget
from classes.analysis.inv_analysis_chart import AnalysisChartPlot
from classes.analysis.inv_analysis_chart_3d import AnalysisChart3DPlot

class AnalysisChartCommon(QTabWidget):
    def __init__(self, parent=None):
        super(AnalysisChartCommon, self).__init__(parent)

        self.tab1 = QWidget()
        self.tab2 = QWidget()

        self.addTab(self.tab1, "Tab 1")
        self.addTab(self.tab2, "Tab 2")
        self.setStyleSheet("background-color: #F0F0F0")
        self.setTabText(0, "Chart 2D")

        self.chartPlot = AnalysisChartPlot()
        self.tab1.setLayout(self.chartPlot)

        self.chartPlot2 = AnalysisChart3DPlot()
        self.setTabText(1, "Chart 3D")
        self.tab2.setLayout(self.chartPlot2)


    def drawChart(self, xVlv, yVlv, xlbl, ylbl, id):
        self.chartPlot.drawChart(xVlv, yVlv, xlbl, ylbl, id)

    def drawChart3D(self, xVlv, yVlv, zVlv, xlbl, ylbl, zlbl, id):
        self.chartPlot2.drawChart(xVlv, yVlv, zVlv, xlbl, ylbl, zlbl, id)
