from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem, QHeaderView

class Sheet(QTableWidget):
    def __init__(self):
        super().__init__()

        self.setRowCount(10)
        self.setColumnCount(0)
        self.numOfCol = 0
        self.show()

    def set_data(self, vlv):
        self.setRowCount(len(vlv)-1)
        self.setColumnCount(len(vlv[0]))
        self.setHorizontalHeaderLabels(vlv[0])
        self.numOfCol = self.columnCount()
        del vlv[0]

        for column in range(len(vlv[0])):
            for row in range(len(vlv)):
                self.setItem(row, column, QTableWidgetItem(vlv[row][column]))

        self.horizontalHeader().setStretchLastSection(True)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def getColumnValues(self, col1Num, col2Num):
        col1Vlv = []
        col2Vlv = []

        for row in range(self.rowCount()):
            try:
                vl1 = float(self.item(row, col1Num).text())
                vl2 = float(self.item(row, col2Num).text())
                col1Vlv.append(vl1)
                col2Vlv.append(vl2)
            except:
                print(str(row) + " " + str(col1Num))
        return col1Vlv, col2Vlv

    def getColumnValue(self, col1Num):
        col1Vlv = []

        for row in range(self.rowCount()):
            try:
                vl1 = float(self.item(row, col1Num).text())
                col1Vlv.append(vl1)
            except:
                print(str(row) + " " + str(col1Num))

        return col1Vlv

    def getNamesOfColums(self):
        headerName = []
        for i in range(self.columnCount()):
            headerName.append(self.horizontalHeaderItem(i).text())
        return headerName

    def getNumOfColumn(self):
        return self.columnCount()

    def hideUnhiddenColumn(self, colID, value):
        self.setColumnHidden(colID, value)