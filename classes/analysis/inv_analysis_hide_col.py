from PyQt5.QtWidgets import QVBoxLayout, QGroupBox, QLabel, QCheckBox
import collections

class AnalysisHideCol(QGroupBox):
    def __init__(self, sheet):
        super().__init__()

        self.sheet = sheet
        self.setTitle("Columns")
        self.layout = QVBoxLayout()

        self.label2 = QLabel("Empty data!")
        self.layout.addWidget(self.label2)

        self.updateData()

    def updateData(self):
        numOfColumns = self.sheet.getNumOfColumn()

        if numOfColumns > 0:
            for i in reversed(range(self.layout.count())):
                self.layout.itemAt(i).widget().setParent(None)

            names = self.sheet.getNamesOfColums()

            gruopBox = QGroupBox()
            self.checkboxSet = collections.OrderedDict()


            for i in range(numOfColumns):
                widget = QCheckBox(names[i], gruopBox)
                widget.setChecked(True)
                widget.stateChanged.connect(self.show_state)
                self.layout.addWidget(widget)

                self.checkboxSet[i] = widget

        self.setLayout(self.layout)
        self.update()


    def show_state(self):
        for num, label in self.checkboxSet.items():
            self.sheet.hideUnhiddenColumn(num, not label.isChecked())