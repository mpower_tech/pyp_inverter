from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel, QPushButton, QVBoxLayout, QLineEdit, QFileDialog, \
    QMessageBox
import os, csv

class AnalysisReadFile(QGroupBox):
    def __init__(self, sheet, hideColumns, chartSettings):
        super().__init__()

        self.setTitle("Read file")
        self.tableWidget = sheet
        self.hideColumns = hideColumns
        self.chartSettings = chartSettings

        f = QFont("Helvetica")
        lblSelect = QLabel("Click the button and select file z data to analysis...")
        lblSelect.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        lblSelect.setFont(f)

        btnSelectFile = QPushButton("Select")
        btnSelectFile.clicked.connect(self.__openFile)

        self.fileName = QLineEdit()
        self.fileName.setAlignment(Qt.AlignTop)
        self.fileName.setReadOnly(True)

        layoutbtnandlineEdit = QHBoxLayout()
        layoutbtnandlineEdit.addWidget(self.fileName)
        layoutbtnandlineEdit.addWidget(btnSelectFile)

        layout = QVBoxLayout()
        layout.addWidget(lblSelect)
        layout.addLayout(layoutbtnandlineEdit)

        self.setLayout(layout)

    def __openFile(self):
        path = QFileDialog.getOpenFileName(self, 'Open CSV', os.getenv('HOME'), 'CSV(*.csv)')
        if path[0] != '':
            self.fileName.setText(path[0])
            with open(path[0], "r") as fileInput:
                vlv = []
                my_file = csv.reader(fileInput, delimiter=';', quotechar='|')
                for row_data in my_file:
                    vlv.append(row_data)

                self.tableWidget.set_data(vlv)
                self.hideColumns.updateData()
                self.chartSettings.updateData()


                QMessageBox.information(self, "Data loaded", "The data has been loaded correctly.")